/*
The MIT License (MIT)

Copyright (c) 2015 Julius Ikkala

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#ifndef UGB_FIFO_H_
#define UGB_FIFO_H_
    #include <stddef.h>
    #include <stdlib.h>
    #include <string.h>

    /*Implemented as a ring buffer. Do not access the elements directly.*/
    struct ugb_fifo
    {
        /*Contains the data, sequentially.*/
        void *array;

        /*Amount of elements in the array*/
        size_t size;
        /*Maximum amount of elements in the array, the array has been allocated
          to contain this many elements*/
        size_t alloc_size;
        /*Size of each element in bytes*/
        size_t element_size;
        /*Head of the buffer*/
        size_t head;
    };
    static inline struct ugb_fifo ugb_create_fifo(
        size_t max_elements,
        size_t element_size
    ){
        struct ugb_fifo res;
        res.array=malloc(element_size*max_elements);
        res.element_size=element_size;
        res.alloc_size=max_elements;
        res.size=0;
        res.head=0;
        return res;
    }
    static inline void ugb_free_fifo(struct ugb_fifo* fifo)
    {
        if(fifo->array!=NULL)
        {
            free(fifo->array);
            fifo->array=NULL;
        }
        fifo->size=0;
        fifo->alloc_size=0;
    }
    static inline void ugb_fifo_clear(struct ugb_fifo* fifo)
    {
        fifo->size=0;
        fifo->head=0;
    }
    /*Returns the number of elements in the fifo*/
    static inline size_t ugb_fifo_size(const struct ugb_fifo* fifo)
    {
        return fifo->size;
    }
    /*Returns the current maximum size*/
    static inline size_t ugb_fifo_get_max_size(const struct ugb_fifo* fifo)
    {
        return fifo->alloc_size;
    }
    /*Returns the old maximum size*/
    static inline size_t ugb_fifo_set_max_size(
        struct ugb_fifo* fifo,
        size_t max_elements
    ){
        size_t old_max_size=fifo->alloc_size;
        if(max_elements==0)
        {
            ugb_free_fifo(fifo);
        }
        else if(old_max_size==0)
        {
            fifo->array=malloc(max_elements*fifo->element_size);
        }
        /*If the fifo is shrunk, adjust elements beforehand*/
        else if(max_elements<old_max_size)
        {
            size_t removed_elements=old_max_size-max_elements;
            size_t new_head=removed_elements<fifo->head?
                fifo->head-removed_elements
                :
                0;
            memmove(
                (char*)fifo->array+new_head*fifo->element_size,
                (char*)fifo->array+fifo->head*fifo->element_size,
                (fifo->alloc_size-fifo->head)*fifo->element_size
            );
            fifo->head=new_head;
            fifo->array=realloc(fifo->array, max_elements*fifo->element_size);
            if(fifo->size>max_elements)
            {
                fifo->size=max_elements;
            }
        }
        /*Else, add space before head*/
        else if(old_max_size<max_elements)
        {
            size_t new_head=max_elements-fifo->alloc_size+fifo->head;
            fifo->array=realloc(fifo->array, max_elements*fifo->element_size);
            memmove(
                (char*)fifo->array+new_head*fifo->element_size,
                (char*)fifo->array+fifo->head*fifo->element_size,
                (fifo->alloc_size-fifo->head)*fifo->element_size
            );
            fifo->head=new_head;
        }
        fifo->alloc_size=max_elements;
        return old_max_size;
    }
    static inline void* ugb_fifo_index(struct ugb_fifo* fifo, size_t index)
    {
        size_t byte_offset=0;
        if(fifo->size<=index)
        {
            return NULL;
        }
        byte_offset=fifo->element_size*(
            (fifo->head+index)%fifo->alloc_size
        );
        return (void*)((char*)fifo->array+byte_offset);
    }
    /*Returns a pointer to the first element of the fifo*/
    static inline void* ugb_fifo_head(struct ugb_fifo* fifo)
    {
        return ugb_fifo_index(fifo, 0);
    }
    /*Returns a pointer to the last element of the fifo*/
    static inline void* ugb_fifo_tail(struct ugb_fifo* fifo)
    {
        return ugb_fifo_index(fifo, fifo->size-1);
    }

    /*Pushes the element to the front of the fifo*/
    static inline void ugb_fifo_push(struct ugb_fifo* fifo, const void* element)
    {
        if(fifo->head==0)
        {
            fifo->head=fifo->alloc_size-1;
        }
        else
        {
            fifo->head--;
        }
        if(fifo->alloc_size>fifo->size)
        {
            fifo->size++;
        }
        /*Copy the element to its place*/
        memmove(
            (char*)fifo->array+fifo->head*fifo->element_size,
            element,
            fifo->element_size
        );
    }
    /*Pops an element from the end of the fifo, and writes it to 'element'
      unless it is NULL.*/
    static inline void ugb_fifo_pop(struct ugb_fifo* fifo, void* element)
    {
        if(fifo->size==0)
        {
            return;
        }
        if(element!=NULL)
        {
            memmove(element, ugb_fifo_tail(fifo), fifo->element_size);
        }
        fifo->size--;
    }
    /*Fills out 'element' with the erased data unless it is NULL.*/
    static inline void ugb_fifo_erase(
        struct ugb_fifo* fifo,
        size_t index,
        void* element
    ){
        size_t i=0;
        if(index>=fifo->size)
        {
            return;
        }
        if(element!=NULL)
        {
            memmove(element, ugb_fifo_index(fifo, index), fifo->element_size);
        }
        for(i=index;i<fifo->size-1;++i)
        {
            memmove(
                ugb_fifo_index(fifo, i),
                ugb_fifo_index(fifo, i+1),
                fifo->element_size
            );
        }
        fifo->size--;
    }
#endif
