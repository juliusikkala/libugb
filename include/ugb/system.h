/*
The MIT License (MIT)

Copyright (c) 2015 Julius Ikkala

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#ifndef UGB_SYSTEM_H_
#define UGB_SYSTEM_H_
    #include "cartridge.h"
    #include "lcd.h"
    #include "timer.h"
    #include "pad.h"
    #include "cpu.h"
    #include "bits.h"
    #include <time.h>
    #define UGB_ROM_ADDRESS                           0x0000
    #define UGB_ROM_BANK0_ADDRESS                     0x0000
    #define UGB_ROM_BANKN_ADDRESS                     0x4000
    #define UGB_TILE_RAM_ADDRESS                      0x8000
    #define UGB_TILE_MAP1_ADDRESS                     0x9800
    #define UGB_TILE_MAP2_ADDRESS                     0x9C00
    #define UGB_EXT_RAM_BANK_ADDRESS                  0xA000
    #define UGB_INT_RAM_ADDRESS                       0xC000
    #define UGB_ECHO_RAM_ADDRESS                      0xE000
    #define UGB_OAM_ADDRESS                           0xFE00
    #define UGB_RESERVED_ADDRESS                      0xFEA0
    #define UGB_IO_ADDRESS                            0xFF00
    #define UGB_ZERO_PAGE_ADDRESS                     0xFF80
    #define UGB_INTERRUPT_ENABLE_ADDRESS              0xFFFF

    #define UGB_VRAM_SIZE                             0x2000
    #define UGB_OAM_SIZE                              0xA0
    #define UGB_ZERO_PAGE_SIZE                        0x80
    
    struct ugb_memory
    {
        uint8_t vram[UGB_VRAM_SIZE];
        uint8_t ram[UGB_RAM_BANK_SIZE];
        uint8_t oam[UGB_OAM_SIZE];
        uint8_t zero_page[UGB_ZERO_PAGE_SIZE];
    };
    typedef void (*ugb_save_callback)(
        void* userdata,
        const uint8_t* ram,
        size_t ram_sz
    );
    struct ugb_system
    {
        struct ugb_memory* mem;
        struct ugb_cartridge* cart;
        struct ugb_lcd* lcd;
        struct ugb_timer timer;
        struct ugb_cpu cpu;
        struct ugb_pad pad;
        
        /*TODO: implement serial port*/
        struct {
            uint8_t data;
            UGB_BYTE_STRUCT
            (
                uint8_t shift_clock:1;
                uint8_t padding:6;
                uint8_t transfer_start:1;
            ) control;
        } serial;

        /*Callbacks*/
        void* userdata;
        ugb_save_callback save_cb;
    };
    struct ugb_system ugb_create_system(
        struct ugb_cartridge *cart,
        struct ugb_lcd *lcd
    );
    void ugb_reset_system(struct ugb_system* sys);
    void ugb_free_system(struct ugb_system* sys);
    
    uint8_t ugb_get8(const struct ugb_system* sys, uint16_t addr);
    void ugb_set8(struct ugb_system* sys, uint16_t addr, uint8_t val);
    uint16_t ugb_get16(const struct ugb_system* sys, uint16_t addr);
    void ugb_set16(struct ugb_system* sys, uint16_t addr, uint16_t val);

    void ugb_set_callbacks(
        struct ugb_system* sys,
        void* userdata,
        ugb_save_callback save_cb
    );
    
    /*Returns the amount of cycles spent.*/
    unsigned ugb_step(struct ugb_system* sys);
    void ugb_step_frame(struct ugb_system* sys);
#endif
