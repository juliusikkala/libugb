/*
The MIT License (MIT)

Copyright (c) 2015 Julius Ikkala

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#ifndef UGB_LCD_H_
#define UGB_LCD_H_
    #include "bits.h"
    #define UGB_HBLANK                                0
    #define UGB_VBLANK                                1
    #define UGB_LCDC_OAM                              2
    #define UGB_LCDC_VRAM_OAM                         3
    
    #define UGB_DISPLAY_WIDTH                         160
    #define UGB_DISPLAY_HEIGHT                        144
    #define UGB_TILE_BYTES                            16
    #define UGB_TILE_WIDTH                            8
    #define UGB_TILE_HEIGHT                           8
    #define UGB_BACKGROUND_WIDTH                      32
    #define UGB_BACKGROUND_HEIGHT                     32
    #define UGB_SCANLINE_BYTES                        40
    #define UGB_SCANLINE_SPRITES                      10
    #define UGB_SPRITE_NUMBER                         40
    #define UGB_SPRITE_X_OFFSET                       8
    #define UGB_SPRITE_Y_OFFSET                       16

    #define UGB_HBLANK_CYCLES        51
    #define UGB_LCDC_OAM_CYCLES      20
    #define UGB_LCDC_VRAM_OAM_CYCLES 43
    #define UGB_VBLANK_CYCLES        1140
    #define UGB_SCANLINE_CYCLES \
        (UGB_HBLANK_CYCLES+UGB_LCDC_OAM_CYCLES+UGB_LCDC_VRAM_OAM_CYCLES)
    /*Yields a framerate of 59.7275...*/
    #define UGB_LCD_REFRESH_CYCLES \
        (UGB_DISPLAY_HEIGHT*UGB_SCANLINE_CYCLES+UGB_VBLANK_CYCLES)
    /*Contains the vblank scanlines*/
    #define UGB_SCANLINES 154
    #define UGB_FRAME_MS (UGB_LCD_REFRESH_CYCLES*1000/UGB_CLOCK_SPEED_HZ)
    #define UGB_DISPLAY_SIZE \
        (UGB_DISPLAY_WIDTH*UGB_DISPLAY_HEIGHT*sizeof(struct ugb_color))

    struct ugb_system;
    struct ugb_memory;

    struct ugb_color
    {
        uint8_t r, g, b;
    };
    
    struct ugb_sprite
    {
        uint8_t pos_y;
        uint8_t pos_x;
        uint8_t tile_index;
        struct
        {
            uint8_t padding:4;
            uint8_t palette_index:1;
            uint8_t x_flip:1;
            uint8_t y_flip:1;
            uint8_t priority:1;
        } flags;
    };
    
    struct ugb_lcd
    {
        struct ugb_color* display;
        /*Palette used to draw to the display.*/
        struct ugb_color palette[4];
        /*The sprites from OAM that need to be rendered during the current
          scanline. Read and sorted during mode 2.*/
        struct ugb_sprite sprites[UGB_SCANLINE_SPRITES];
        /*Cycles left of current mode.*/
        unsigned cycles_left;
        
        uint8_t scanline;
        uint8_t interrupt_scanline;
        /*Used for rendering the window properly. Once this is
          >= UGB_DISPLAY_HEIGHT, the window is no longer rendered.*/
        uint8_t window_scanline;
        /*window_y changes aren't supposed to be dynamic, so we store the
          value in case window_y is changed during the frame.*/
        uint8_t old_window_y;
        uint8_t window_y, window_x;
        uint8_t scroll_y, scroll_x;
        
        UGB_BYTE_STRUCT
        (
            uint8_t background_display:1;
            uint8_t sprite_display:1;
            uint8_t sprite_size:1;    /*0 -> 8x8, 1 -> 8x16*/
            uint8_t background_tile_map:1;    /*0 -> 0x9800, 1 -> 0x9C00*/
            uint8_t tileset_select:1; /*0 -> 0x8800, 1 -> 0x8000*/
            uint8_t window_display:1;
            uint8_t window_tile_map:1;/*0 -> 0x9800, 1 -> 0x9C00*/
            uint8_t lcd_on:1;
        ) control;
        UGB_BYTE_STRUCT
        (
            /*0 -> hblank
              1 -> vblank
              2 -> oam in use (unaccessible)
              3 -> oam and vram in use (unaccessible)*/
            uint8_t mode:2;
            uint8_t scanline_coincidence:1;
            uint8_t interrupt_mode00:1;
            uint8_t interrupt_mode01:1;
            uint8_t interrupt_mode10:1;
            uint8_t scanline_coincidence_enabled:1;
            uint8_t padding:2;
        ) status;
        UGB_BYTE_STRUCT
        (
            /*0 -> lightest, 3 -> darkest*/
            uint8_t c00:2;
            uint8_t c01:2;
            uint8_t c10:2;
            uint8_t c11:2;
        ) tile_palette;
        UGB_BYTE_STRUCT
        (
            /*0 -> transparent, 1 -> lightest, 3 -> darkest*/
            uint8_t c00:2;
            uint8_t c01:2;
            uint8_t c10:2;
            uint8_t c11:2;
        ) sprite_palette[2];
    };
    static const struct ugb_color ugb_default_palette[4]={
        {255,255,255}, {208,208,208}, {147,147,147}, {0,0,0}
    };
    
    struct ugb_lcd ugb_create_lcd(const struct ugb_color palette[4]);
    void ugb_reset_lcd(struct ugb_lcd* lcd);
    void ugb_free_lcd(struct ugb_lcd* lcd);
    /*If you turn the lcd manually off, call this function first to set the
      correct mode.*/
    void ugb_lcd_power(struct ugb_lcd* lcd, unsigned lcd_on);
    void ugb_lcd_update(
        struct ugb_lcd* lcd,
        struct ugb_system* sys,
        unsigned cycles
    );
#endif
