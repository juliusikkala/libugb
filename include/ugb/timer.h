/*
The MIT License (MIT)

Copyright (c) 2015 Julius Ikkala

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#ifndef UGB_TIMER_H_
#define UGB_TIMER_H_
    #include "bits.h"
    #define UGB_TIMER00_CYCLES       256
    #define UGB_TIMER01_CYCLES       4
    #define UGB_TIMER10_CYCLES       16
    #define UGB_TIMER11_CYCLES       64
    
    struct ugb_system;
    struct ugb_timer
    {
        uint8_t ticks;
        uint8_t divider;
        uint16_t cycles;
        uint8_t divider_cycles;
        uint8_t modulo;
        UGB_BYTE_STRUCT
        (
            /*0 ->   4096 Hz
              1 -> 262144 Hz
              2 ->  65536 HZ
              3 ->  16384 Hz*/
            uint8_t clock_freq:2;
            uint8_t timer_run:1;/*0 -> stopped, 1 -> running*/
            uint8_t padding:6;
        ) control;
    };
    struct ugb_timer ugb_create_timer();
    void ugb_timer_reset(struct ugb_timer* timer);
    void ugb_timer_cycle(struct ugb_timer* timer, struct ugb_system* sys);
    void ugb_timer_update(
        struct ugb_timer* timer,
        struct ugb_system* sys,
        unsigned spent_cycles
    );
#endif
