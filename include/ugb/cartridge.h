/*
The MIT License (MIT)

Copyright (c) 2015 Julius Ikkala

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#ifndef UGB_CARTRIDGE_H_
#define UGB_CARTRIDGE_H_
    #include "bits.h"
    #include <time.h>
    #define UGB_VBLANK_INTERRUPT_ADDRESS              0x0040
    #define UGB_LCDC_INTERRUPT_ADDRESS                0x0048
    #define UGB_TIMER_OVERFLOW_INTERRUPT_ADDRESS      0x0050
    #define UGB_SERIAL_TRANSFER_INTERRUPT_ADDRESS     0x0058
    #define UGB_PAD_EVENT_INTERRUPT_ADDRESS           0x0060
    #define UGB_ENTRY_POINT_ADDRESS                   0x0100
    #define UGB_HEADER_ADDRESS                        0x0104
    #define UGB_LOGO_ADDRESS                          0x0104
    #define UGB_GAME_TITLE_ADDRESS                    0x0134
    #define UGB_GBC_FLAG_ADDRESS                      0x0143
    #define UGB_NEW_LICENSEE_ADDRESS                  0x0144
    #define UGB_SGB_FLAG_ADDRESS                      0x0146
    #define UGB_CARTRIDGE_TYPE_ADDRESS                0x0147
    #define UGB_ROM_SIZE_ADDRESS                      0x0148
    #define UGB_RAM_SIZE_ADDRESS                      0x0149
    #define UGB_LOCALE_ADDRESS                        0x014A
    #define UGB_OLD_LICENSEE_ADDRESS                  0x014B
    #define UGB_ROM_VERSION_ADDRESS                   0x014C
    #define UGB_COMPLEMENT_CHECK_ADDRESS              0x014D
    #define UGB_CHECKSUM_ADDRESS                      0x014E
    #define UGB_EXECUTABLE_ADDRESS                    0x0150
    
    #define UGB_CARTRIDGE_IO0                         0x0000
    #define UGB_CARTRIDGE_IO1                         0x2000
    #define UGB_CARTRIDGE_IO2                         0x4000
    #define UGB_CARTRIDGE_IO3                         0x6000

    #define UGB_ROM_BANK_SIZE                         0x4000
    #define UGB_RAM_BANK_SIZE                         0x2000
    
    #define UGB_NO_MBC_MAX_RAM_BANKS                  0x02
    #define UGB_MBC1_MAX_RAM_BANKS                    0x04
    #define UGB_MBC2_MAX_RAM_BANKS                    0x01
    #define UGB_MBC3_MAX_RAM_BANKS                    0x04
    #define UGB_MBC5_MAX_RAM_BANKS                    0x10
    
    #define UGB_GBC_FLAG                              0xC0
    #define UGB_GB_GBC_FLAG                           0x80
    #define UGB_GB_FLAG                               0x00
    #define UGB_SGB_SUPPORT_FLAG                      0x03
    #define UGB_NO_SGB_SUPPORT_FLAG                   0x00
    #define UGB_LOCALE_JAPANESE                       0x00
    #define UGB_LOCALE_ENGLISH                        0x01
    #define UGB_LICENSEE_IS_NEW                       0x33

    enum ugb_mbc_type
    {
        UGB_NO_MBC=0,
        UGB_MBC1,
        UGB_MBC2,
        UGB_MBC3,
        UGB_MBC5
    };

    struct ugb_mbc
    {
        size_t rom_sz;/*in banks*/
        size_t ram_sz;/*in banks*/
        size_t rom_bank;
        /*If MBC3, values 0x08-0x0C are used to signify RTC registers*/
        size_t ram_bank;
        uint8_t ram_enable:1;
        enum ugb_mbc_type type;
        union {
            struct {
                uint8_t rom_ram_mode:1;
            } mbc1;
            struct {
                /*Because the halt is done by writing 0 and then 1, this
                  variable can store the intermediate state of 0 written, 1 not
                  yet written.
                  0   -> halted
                  1   -> 0 written
                  2,3 -> not halted*/
                uint8_t rtc_read_halt:2;
                uint8_t rtc_write_halt:1;
                /*Stores the state of the RTC when it is halted.*/
                time_t halt_time;
            } mbc3;
        } data;
    };
    
    typedef uint8_t ugb_rom_bank[UGB_ROM_BANK_SIZE];
    typedef uint8_t ugb_ram_bank[UGB_RAM_BANK_SIZE];
    struct ugb_cartridge
    {
        ugb_rom_bank *rom;
        ugb_ram_bank *ram;
        struct ugb_mbc mbc;
    };
    
    struct ugb_mbc ugb_create_mbc(const ugb_rom_bank *bank0);
    /*Load the rom file, then give it to this function. This function will only
      copy the data, it won't take ownership.*/
    struct ugb_cartridge ugb_create_cartridge(
        const uint8_t *rom_data,
        size_t rom_data_sz,/*In bytes*/
        const uint8_t *ram_data,/*If NULL, RAM is initialized to zeroes.*/
        size_t ram_data_sz/*In bytes*/
    );
    /*Resets the cartridge (does not clear RAM)*/
    void ugb_reset_cartridge(struct ugb_cartridge* cart);
    void ugb_free_cartridge(struct ugb_cartridge* cart);
    unsigned ugb_cartridge_ok(struct ugb_cartridge* cart);
    /*The size of the returned array is UGB_RAM_BANK_SIZE. This function is
      provided so that the RAM can be saved.*/
    const uint8_t* ugb_cartridge_get_ram(struct ugb_cartridge* cart);
    void ugb_cartridge_write(
        struct ugb_cartridge* cart,
        uint16_t addr,
        uint8_t val
    );
    uint8_t ugb_cartridge_read(
        const struct ugb_cartridge* cart,
        uint16_t addr
    );
#endif
