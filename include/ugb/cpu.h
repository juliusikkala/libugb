/*
The MIT License (MIT)

Copyright (c) 2015 Julius Ikkala

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#ifndef UGB_CPU_H_
#define UGB_CPU_H_
    #include "bits.h"
    #define UGB_ZERO_FLAG                             0x80
    #define UGB_SUBTRACT_FLAG                         0x40
    #define UGB_HALF_CARRY_FLAG                       0x20
    #define UGB_CARRY_FLAG                            0x10
    
    #define UGB_ZERO_FLAG_OFFSET                      0x7
    #define UGB_SUBTRACT_FLAG_OFFSET                  0x6
    #define UGB_HALF_CARRY_FLAG_OFFSET                0x5
    #define UGB_CARRY_FLAG_OFFSET                     0x4

    #define UGB_CLOCK_SPEED_HZ       1048576

    /*Stores all CPU registers (not IO registers)*/
    struct ugb_cpu
    {
        uint16_t sp, pc;
        uint8_t a, f, b, c, d, e, h, l;
        uint8_t halted:1;
        uint8_t stopped:1;

        uint8_t int_master:1;
        UGB_BYTE_STRUCT
        (
            uint8_t vblank:1;
            uint8_t lcdc:1;
            uint8_t timer_overflow:1;
            uint8_t serial_io_complete:1;
            uint8_t pad_event:1;
            uint8_t padding:3;
        ) int_flag;
        UGB_BYTE_STRUCT
        (
            uint8_t vblank:1;
            uint8_t lcdc:1;
            uint8_t timer_overflow:1;
            uint8_t serial_io_complete:1;
            uint8_t pad_event:1;
            uint8_t padding:3;
        ) int_enable;
    };
    struct ugb_system;
    /*Increments sys->cpu.pc as needed. Returns the number of cycles spent on
      the instruction. Increments the timer accordingly.*/
    unsigned ugb_execute(struct ugb_system* sys);
    struct ugb_cpu ugb_create_cpu();
#endif
