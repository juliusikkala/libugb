/*
The MIT License (MIT)

Copyright (c) 2015 Julius Ikkala

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#ifndef UGB_PAD_H_
#define UGB_PAD_H_
    #include "bits.h"
    #define UGB_BUTTON_RIGHT        0
    #define UGB_BUTTON_LEFT         1
    #define UGB_BUTTON_UP           2
    #define UGB_BUTTON_DOWN         3
    #define UGB_BUTTON_A            4
    #define UGB_BUTTON_B            5
    #define UGB_BUTTON_SELECT       6
    #define UGB_BUTTON_START        7
    #define UGB_BUTTON_PRESS        0
    #define UGB_BUTTON_RELEASE      1
    
    struct ugb_pad
    {
        /*These represent the physical buttons on the device.
          0 -> pressed,
          1 -> released.*/
        /*row 0 (P14)*/
        uint8_t right:1;  /*P10*/
        uint8_t left:1;   /*P11*/
        uint8_t up:1;     /*P12*/
        uint8_t down:1;   /*P13*/
        /*row 1 (P15)*/
        uint8_t a:1;      /*P10*/
        uint8_t b:1;      /*P11*/
        uint8_t select:1; /*P12*/
        uint8_t start:1;  /*P13*/

        /*Set to zero to select*/
        UGB_BYTE_STRUCT
        (
            uint8_t p10:1;
            uint8_t p11:1;
            uint8_t p12:1;
            uint8_t p13:1;
            uint8_t p14:1;
            uint8_t p15:1;
            uint8_t padding:2;
        ) p1;
    };
    
    struct ugb_system;
    struct ugb_pad ugb_create_pad();
    void ugb_pad_update(struct ugb_pad* pad, struct ugb_system* sys);
    void ugb_pad_event(
        struct ugb_system* sys,
        unsigned button,
        unsigned event
    );
#endif
