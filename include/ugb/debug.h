/*
The MIT License (MIT)

Copyright (c) 2015 Julius Ikkala

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#ifndef UGB_DEBUG_H_
#define UGB_DEBUG_H_
    #include "cartridge.h"
    #include "system.h"
    #include "fifo.h"
    #include "bits.h"
    
    #define UGB_UNTIL_VBLANK              0x1
    #define UGB_UNTIL_LCDC                0x2
    #define UGB_UNTIL_TIMER_OVERFLOW      0x4
    #define UGB_UNTIL_SERIAL_IO_COMPLETE  0x8
    #define UGB_UNTIL_PAD_EVENT           0x10
    #define UGB_UNTIL_INTERRUPT  \
        (UGB_UNTIL_VBLANK|UGB_UNTIL_LCDC|\
        UGB_UNTIL_TIMER_OVERFLOW|UGB_UNTIL_SERIAL_IO_COMPLETE|\
        UGB_UNTIL_PAD_EVENT)
    #define UGB_UNTIL_OUT_OF_LOOP         0x20
    #define UGB_UNTIL_INVALID_PC          0x40
    #define UGB_UNTIL_INVALID_SP          0x80
    #define UGB_UNTIL_INVALID_INSTRUCTION 0x100
    #define UGB_UNTIL_INVALID_STATE (UGB_UNTIL_INVALID_PC|UGB_UNTIL_INVALID_SP)
    /*unsigned arg1; the stopping number for PC*/
    #define UGB_UNTIL_PC                  0x200
    /*unsigned arg1: the number of steps to step*/
    #define UGB_UNTIL_STEPS               0x400
    /*unsigned arg1: the minimum number of cycles to go*/
    #define UGB_UNTIL_CYCLES              0x800
    /*unsigned arg1: the memory address to track*/
    #define UGB_UNTIL_CHANGE              0x1000
    #define UGB_DEFAULT_COMMAND_HISTORY_MAX_SZ 128
    #define UGB_DEFAULT_STATE_HISTORY_MAX_SZ   1024
    #define UGB_INSTRUCTION_MAX_STRLEN 16

    extern const char * const ugb_instruction_strings[256];
    extern const char * const ugb_instruction_cb_strings[256];
    
    struct ugb_debug
    {
        struct ugb_system* sys;
        struct ugb_fifo command_history;
        struct ugb_fifo state_history;
        unsigned verbosity;
        
        uint8_t enabled:1;
    };
    
    
    /*Reads the instruction starting from 'address' and writes the formatted
      string to 'str', which must contain >= UGB_INSTRUCTION_MAX_STRLEN bytes,
      unless you like to live dangerously.
      Example:
      Address points to 0x08 0x10 0x15.
      When this function returns, str contains "LD (0x1510), SP"
      Note that the function does not write a newline.*/
    void ugb_instruction_string(
        const struct ugb_system* sys,
        uint16_t address,
        char *str
    );
    unsigned ugb_pc_valid(uint16_t pc);
    unsigned ugb_sp_valid(uint16_t sp);
    unsigned ugb_instruction_valid(uint8_t opcode);
    /*verbosity==0 -> Nothing is shown
      verbosity==1 -> Only the instruction and PC are printed
      verbosity==2 -> A, B, C, D, E, H, L, F, SP are additionally printed
      verbosity==3 -> CPU modes are printed
      verbosity==4 -> Cartridge state is additionally printed*/
    void ugb_print_system_state(
        const struct ugb_system* sys,
        unsigned verbosity
    );
    void ugb_print_cartridge_info(const struct ugb_cartridge* cart);
    void ugb_print_mbc_state(const struct ugb_mbc* mbc);

    /*Returns the flags that caused the function to exit*/
    uint32_t ugb_step_until(struct ugb_debug* debug, uint32_t until, ...);

    struct ugb_debug ugb_create_debug(struct ugb_system* sys);
    void ugb_free_debug(struct ugb_debug* debug);
    /*Reads and executes one command from stdin, then returns.*/
    void ugb_step_debug(struct ugb_debug* debug);
#endif
