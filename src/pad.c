/*
The MIT License (MIT)

Copyright (c) 2015 Julius Ikkala

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "pad.h"
#include "system.h"

struct ugb_pad ugb_create_pad()
{
    struct ugb_pad pad;
    pad.right=1;
    pad.left=1;
    pad.up=1;
    pad.down=1;
    pad.a=1;
    pad.b=1;
    pad.select=1;
    pad.start=1;
    pad.p1.byte=0xFF;
    return pad;
}
void ugb_pad_update(struct ugb_pad* pad, struct ugb_system* sys)
{
    if(pad->p1.fields.p14==0)
    {
        if(pad->right!=pad->p1.fields.p10||
           pad->left!=pad->p1.fields.p11||
           pad->up!=pad->p1.fields.p12||
           pad->down!=pad->p1.fields.p13)
        {
            sys->cpu.int_flag.fields.pad_event=1;
        }
        pad->p1.fields.p10=pad->right;
        pad->p1.fields.p11=pad->left;
        pad->p1.fields.p12=pad->up;
        pad->p1.fields.p13=pad->down;
    }
    else if(pad->p1.fields.p15==0)
    {
        if(pad->a!=pad->p1.fields.p10||
           pad->b!=pad->p1.fields.p11||
           pad->select!=pad->p1.fields.p12||
           pad->start!=pad->p1.fields.p13)
        {
            sys->cpu.int_flag.fields.pad_event=1;
        }
        pad->p1.fields.p10=pad->a;
        pad->p1.fields.p11=pad->b;
        pad->p1.fields.p12=pad->select;
        pad->p1.fields.p13=pad->start;
    }
}

void ugb_pad_event(
    struct ugb_system* sys,
    unsigned button,
    unsigned event
){
    switch(button)
    {
    case UGB_BUTTON_RIGHT:
        sys->pad.right=event;
        break;
    case UGB_BUTTON_LEFT:
        sys->pad.left=event;
        break;
    case UGB_BUTTON_UP:
        sys->pad.up=event;
        break;
    case UGB_BUTTON_DOWN:
        sys->pad.down=event;
        break;
    case UGB_BUTTON_A:
        sys->pad.a=event;
        break;
    case UGB_BUTTON_B:
        sys->pad.b=event;
        break;
    case UGB_BUTTON_SELECT:
        sys->pad.select=event;
        break;
    case UGB_BUTTON_START:
        sys->pad.start=event;
        break;
    default:
        break;
    }
    sys->cpu.stopped=0;
    ugb_pad_update(&sys->pad, sys);
}
