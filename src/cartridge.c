/*
The MIT License (MIT)

Copyright (c) 2015 Julius Ikkala

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "cartridge.h"
#include "system.h"
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

struct ugb_mbc ugb_create_mbc(const ugb_rom_bank *bank0)
{
    struct ugb_mbc mbc;
    mbc.rom_bank=1;
    mbc.ram_bank=0;
    mbc.ram_enable=0;
    /*Find the number of ROM banks*/
    switch((*bank0)[UGB_ROM_SIZE_ADDRESS])
    {
    case 0:
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
        mbc.rom_sz=2<<((*bank0)[UGB_ROM_SIZE_ADDRESS]);
        break;
    case 0x52:
        mbc.rom_sz=72;
        break;
    case 0x53:
        mbc.rom_sz=80;
        break;
    case 0x54:
        mbc.rom_sz=96;
        break;
    default:
        mbc.rom_sz=2;
        break;
    }
    /*Find the number of RAM banks*/
    switch((*bank0)[UGB_RAM_SIZE_ADDRESS])
    {
    default:
    case 0:
        mbc.ram_sz=0;
        break;
    case 1:
    case 2:
        mbc.ram_sz=1;
        break;
    case 3:
        mbc.ram_sz=4;
        break;
    case 4:
        mbc.ram_sz=16;
        break;
    }
    /*Select and initialize correct MBC type*/
    switch((*bank0)[UGB_CARTRIDGE_TYPE_ADDRESS])
    {
    case 0x01:
    case 0x02:
    case 0x03:
        mbc.type=UGB_MBC1;
        mbc.data.mbc1.rom_ram_mode=0;
        break;
    case 0x05:
    case 0x06:
        mbc.type=UGB_MBC2;
        break;
    case 0x0F:
    case 0x10:
    case 0x11:
    case 0x12:
    case 0x13:
        mbc.type=UGB_MBC3;
        mbc.data.mbc3.rtc_read_halt=2;
        mbc.data.mbc3.rtc_write_halt=0;
        time(&mbc.data.mbc3.halt_time);
        break;
    case 0x1A:
    case 0x1B:
    case 0x1C:
    case 0x1D:
    case 0x1E:
        mbc.type=UGB_MBC5;
        break;
    default:
        mbc.type=UGB_NO_MBC;
        break;
    }
    return mbc;
}
struct ugb_cartridge ugb_create_cartridge(
    const uint8_t *rom_data,
    size_t rom_data_sz,
    const uint8_t *ram_data,/*If NULL, RAM is initialized to zeroes.*/
    size_t ram_data_sz
)
{
    struct ugb_cartridge cart;
    cart.rom=NULL;
    cart.ram=NULL;
    if(rom_data_sz<UGB_ROM_BANK_SIZE||rom_data==NULL)
    {
        goto fail;
    }
    cart.mbc=ugb_create_mbc((const ugb_rom_bank*)rom_data);
    if(cart.mbc.rom_sz>0)
    {
        size_t rom_bytes=cart.mbc.rom_sz*sizeof(ugb_rom_bank);
        cart.rom=calloc(cart.mbc.rom_sz, sizeof(ugb_rom_bank));
        memcpy(
            cart.rom,
            rom_data,
            rom_data_sz<rom_bytes?rom_data_sz:rom_bytes
        );
    }
    else
    {
        goto fail;
    }
    if(cart.mbc.ram_sz>0)
    {
        size_t ram_bytes=cart.mbc.ram_sz*UGB_RAM_BANK_SIZE;
        cart.ram=calloc(cart.mbc.ram_sz, sizeof(ugb_ram_bank));
        if(ram_data!=NULL)
        {
            memcpy(
                cart.ram,
                ram_data,
                ram_data_sz<ram_bytes?ram_data_sz:ram_bytes
            );
        }
    }
    return cart;
fail:
    ugb_free_cartridge(&cart);
    return cart;
}
void ugb_reset_cartridge(struct ugb_cartridge* cart)
{
    cart->mbc.rom_bank=1;
    cart->mbc.ram_bank=0;
    cart->mbc.ram_enable=0;
    switch(cart->mbc.type)
    {
    case UGB_MBC1:
        cart->mbc.type=UGB_MBC1;
        break;
    case UGB_MBC3:
        cart->mbc.data.mbc3.rtc_read_halt=2;
        cart->mbc.data.mbc3.rtc_write_halt=0;
        time(&cart->mbc.data.mbc3.halt_time);
        break;
    default:
        break;
    }
}
void ugb_free_cartridge(struct ugb_cartridge* cart)
{
    if(cart!=NULL)
    {
        if(cart->rom!=NULL)
        {
            free(cart->rom);
            cart->rom=NULL;
        }
        if(cart->ram!=NULL)
        {
            free(cart->ram);
            cart->ram=NULL;
        }
    }
}
unsigned ugb_cartridge_ok(struct ugb_cartridge* cart)
{
    /*TODO: perform proper integrity checks.*/
    return cart!=NULL&&cart->rom!=NULL;
}
const uint8_t* ugb_cartridge_get_ram(struct ugb_cartridge* cart)
{
    assert(cart!=NULL);
    return cart->ram[0];
}
void ugb_cartridge_write(
    struct ugb_cartridge* cart,
    uint16_t addr,
    uint8_t val
)
{
    assert(cart!=NULL);
    if(addr>=UGB_EXT_RAM_BANK_ADDRESS)
    {
        /*If ram is not enabled, don't allow writing to it.*/
        if(!cart->mbc.ram_enable||cart->mbc.ram_bank>=cart->mbc.ram_sz)
        {
            return;
        }
        switch(cart->mbc.type)
        {
        case UGB_MBC2:
            val&=0xF;
            return;
        case UGB_MBC3:
            if(cart->mbc.ram_bank>0x03)
            {
                /*TODO: support setting rtc*/
                cart->mbc.data.mbc3.rtc_write_halt=(val>>6)&1;
                return;
            }
            break;
        default:
            break;
        }
        cart->ram[cart->mbc.ram_bank][addr-UGB_EXT_RAM_BANK_ADDRESS]=val;
    }
    else if(addr>=UGB_CARTRIDGE_IO3)
    {
        switch(cart->mbc.type)
        {
        case UGB_MBC1:
            cart->mbc.data.mbc1.rom_ram_mode=val&1;
            if(cart->mbc.data.mbc1.rom_ram_mode==1)
            {/*1 ram bank, 128 rom banks*/
                cart->mbc.ram_bank=0;
            }
            else
            {/*4 ram banks, 32 rom banks*/
                cart->mbc.rom_bank&=0x1F;
            }
            break;
        case UGB_MBC3:
            if(val==0)
            {
                cart->mbc.data.mbc3.rtc_read_halt=1;
            }
            else if(val==1&&cart->mbc.data.mbc3.rtc_read_halt==1)
            {
                cart->mbc.data.mbc3.rtc_read_halt=0;
                /*Now that the RTC is supposed to be halted, the current time is
                  stored.*/
                time(&cart->mbc.data.mbc3.halt_time);
            }
            else
            {
                cart->mbc.data.mbc3.rtc_read_halt=2;
            }
            break;
        default:
            break;
        }
    }
    else if(addr>=UGB_CARTRIDGE_IO2)
    {
        switch(cart->mbc.type)
        {
        case UGB_MBC1:
            if(cart->mbc.data.mbc1.rom_ram_mode==1)
            {
                cart->mbc.ram_bank=val&3;
            }
            else
            {
                cart->mbc.rom_bank&=0x1F;
                cart->mbc.rom_bank|=(val&3)<<5;
            }
            break;
        case UGB_MBC3:
        case UGB_MBC5:
            cart->mbc.ram_bank=val&0x0F;
            break;
        default:
            break;
        }
    }
    else if(addr>=UGB_CARTRIDGE_IO1)
    {
        switch(cart->mbc.type)
        {
        case UGB_MBC1:
            if(val%0x20==0)
            {
                val++;
            }
            cart->mbc.rom_bank=val&0x1F;
            break;
        case UGB_MBC2:
            if(((addr>>8)&1)==1)
            {
                if(val==0)
                {
                    val=1;
                }
                cart->mbc.rom_bank=val&0xF;
            }
            break;
        case UGB_MBC3:
            if(val==0)
            {
                val=1;
            }
            cart->mbc.rom_bank=val&0x7F;
            break;
        case UGB_MBC5:
            if(addr>=0x3000)
            {
                /*Set high bit*/
                cart->mbc.rom_bank&=0xFF;
                cart->mbc.rom_bank|=((uint16_t)val&1)<<8;
            }
            else
            {
                /*Set low byte*/
                cart->mbc.rom_bank=val;
            }
            break;
        default:
            break;
        }
    }
    else/*if(addr>=UGB_CARTRIDGE_IO0)*/
    {
        if(cart->mbc.type!=UGB_MBC2||((addr>>8)&1)==0)
        {
            cart->mbc.ram_enable=((val&0x0F)==0x0A);
        }
    }
}
uint8_t ugb_cartridge_read(
    const struct ugb_cartridge* cart,
    uint16_t addr
)
{
    assert(cart!=NULL);
    if(addr>=UGB_EXT_RAM_BANK_ADDRESS)
    {
        /*The bank may actually signify an RTC register*/
        if(
            cart->mbc.type==UGB_MBC3&&
            cart->mbc.ram_bank>=0x08&&
            cart->mbc.ram_bank<0x0C
        ){
            time_t t=time(NULL);
            /*if halted, get the halted time*/
            if(cart->mbc.data.mbc3.rtc_read_halt==0)
            {
                t=cart->mbc.data.mbc3.halt_time;
            }
            struct tm* rtc=localtime(&t);
            switch(cart->mbc.ram_bank)
            {
            case 0x08:
                return (uint8_t)(rtc->tm_sec%60);
            case 0x09:
                return (uint8_t)rtc->tm_min;
            case 0x0A:
                return (uint8_t)rtc->tm_hour;
            case 0x0B:
                return (uint8_t)rtc->tm_yday;
            case 0x0C:
                return (uint8_t)(rtc->tm_yday>>8)|
                       (cart->mbc.data.mbc3.rtc_write_halt<<6);
            }
        }
        /*If ram is not enabled, don't allow reading from it.*/
        if(!cart->mbc.ram_enable||cart->mbc.ram_bank>=cart->mbc.ram_sz)
        {
            return 0;
        }
        return cart->ram[cart->mbc.ram_bank][addr-UGB_EXT_RAM_BANK_ADDRESS];
    }
    else if(addr>=UGB_ROM_BANKN_ADDRESS)
    {
        if(cart->mbc.rom_bank>=cart->mbc.rom_sz)
        {
            return 0;
        }
        return cart->rom[cart->mbc.rom_bank][addr-UGB_ROM_BANKN_ADDRESS];
    }
    else/*if(addr>=UGB_ROM_BANK0_ADDRESS)*/
    {
        return cart->rom[0][addr-UGB_ROM_BANK0_ADDRESS];
    }
    assert(0);
    return 0;
}
