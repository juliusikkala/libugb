/*
The MIT License (MIT)

Copyright (c) 2015 Julius Ikkala

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "debug.h"
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <assert.h>
#define COLOR_DEFAULT        "\x1b[0m"
#define COLOR_RED            "\x1b[31m"
#define COLOR_GREEN          "\x1b[32m"
#define COLOR_YELLOW         "\x1b[33m"
#define COLOR_BLUE           "\x1b[34m"
#define COLOR_MAGENTA        "\x1b[35m"
#define COLOR_CYAN           "\x1b[36m"

struct ugb_state
{
    struct ugb_mbc mbc;
    struct ugb_cpu cpu;
    /*The current opcode is stored, because it may reside outside of ROM and
      thus be changed.*/
    uint8_t opcode[3];
};

static volatile sig_atomic_t exit_debug=0;
static void debug_signal_handler(int signal)
{
    (void)signal;
    exit_debug=1;
}
typedef void (*signal_handler)(int);

static void push_history(struct ugb_fifo* history, char *command)
{
    if(ugb_fifo_size(history)==ugb_fifo_get_max_size(history))
    {
        char *old_command=NULL;
        ugb_fifo_pop(history, &old_command);
        if(old_command!=NULL)
        {
            free(old_command);
        }
    }
    ugb_fifo_push(history, &command);
}
#if defined(__unix__)||(defined(__APPLE__)&&defined(__MACH__))
#include <unistd.h>
#include <termios.h>
#include <sys/ioctl.h>
static size_t strlen_displayed(const char *str)
{
    size_t len=0;
    while(*str!=0)
    {
        if(*str=='\x1b')
        {/*Ansi escape sequence, ends in the first letter*/
            while(*str!=0)
            {
                if(isalpha(*str))
                {
                    str++;
                    break;
                }
                str++;
            }
        }
        str++;
        len++;
    }
    return len;
}
static unsigned get_terminal_width()
{
    struct winsize term;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &term);
    return term.ws_col;
}
static void update_prompt(
    unsigned *line,
    unsigned *cursor_line,
    const char *prompt,
    const char *command,
    unsigned cursor
){
    unsigned terminal_width=get_terminal_width();
    unsigned cursor_x;
    unsigned prompt_length=strlen_displayed(prompt);
    unsigned total_length=prompt_length+strlen_displayed(command);
    unsigned i=0;
    /*Move cursor to the end of this prompt. Note that it is guarded by an if
      because VT100 somehow thinks 0 is 1 (but 1 is still 1).*/
    if(*line>*cursor_line)
    {
        printf("\x1b[%dB", *line-*cursor_line);
        *cursor_line=0;
    }
    /*Erase extra lines one by one*/
    for(i=0;i<*line;i++)
    {
        fputs("\x1b[2K\x1b[1A", stdout);
    }
    /*Clear the row and print the prompt line. There is an extra space to cause
      the terminal to scroll when the line is full.*/
    printf("\x1b[2K\r%s%s \r", prompt, command);
    /*Compute the actual offset of the cursor from the beginning of the file.*/
    cursor+=prompt_length;
    /*Calculate the new last line and the line of the cursor*/
    *line=total_length/terminal_width;
    *cursor_line=cursor/terminal_width;
    /*Get cursor offset from the left edge of the terminal*/
    cursor_x=cursor%terminal_width;
    /*Move the cursor to the correct offset*/
    if(cursor_x>0)
    {
        printf("\x1b[%dC", cursor_x);
    }
    /*And set the correct height for the cursor.*/
    if(*line>*cursor_line)
    {
        printf("\x1b[%dA", *line-*cursor_line);
    }
}
/*Returns NULL on EOF, the new head of the command history otherwise.*/
static unsigned readline(
    const char *prompt,
    struct ugb_fifo* history
){
    struct termios old_term, new_term;
    char *new_command=calloc(1, 1);
    push_history(history, new_command);
    char *current=new_command;
    unsigned history_line=0;
    unsigned exit_status=1;
    int cursor=0;
    int c=0;
    unsigned line=0, cursor_line=0;
    /*Get current terminal settings*/
    tcgetattr(STDIN_FILENO, &old_term);
    new_term=old_term;
    /*Disable canonical mode*/
    new_term.c_lflag&=~ICANON;
    /*Echo must be disabled, we do that manually.*/
    new_term.c_lflag&=~ECHO;
    /*Disallow signals (to get the interrupt character)*/
    new_term.c_lflag&=~ISIG;
    /*Set the interrupt character.*/
    new_term.c_cc[VINTR]='\3';
    /*Apply the new settings*/
    tcsetattr(STDIN_FILENO, TCSANOW, &new_term);
    do
    {
        size_t len=strlen(current);
        update_prompt(
            &line,
            &cursor_line,
            prompt,
            current,
            cursor
        );
        c=getchar();
        switch(c)
        {
        case EOF:
        case '\3':
        case '\4':
            /*EOT/EOF/ETX*/
            free(new_command);
            ugb_fifo_erase(history, 0, NULL);
            exit_status=0;
            history_line=0;
            break;
        case '\b':
        case 0x7F:
            /*Backspace*/
            if(len>0&&cursor>0)
            {
                memmove(
                    current+cursor-1,
                    current+cursor,
                    len-cursor+1
                );
                len--;
                cursor--;
            }
            break;
        case '\x1b':
            /*Escape sequence begins here*/
            c=getchar();
            if(c=='[')
            {/*Still good*/
                c=getchar();
                switch(c)
                {
                case 'A':
                    /*Up, next in history*/
                    if(history_line<ugb_fifo_size(history)-1)
                    {
                        history_line++;
                        current=*(char**)ugb_fifo_index(history, history_line);
                        cursor=strlen(current);
                    }
                    break;
                case 'B':
                    /*Down*/
                    if(history_line>0)
                    {
                        history_line--;
                        current=*(char**)ugb_fifo_index(history, history_line);
                        cursor=strlen(current);
                    }
                    break;
                case 'C':
                    /*Right*/
                    if((size_t)cursor<len)
                    {
                        cursor++;
                    }
                    break;
                case 'D':
                    /*Left*/
                    if(cursor>0)
                    {
                        cursor--;
                    }
                    break;
                default:
                    break;
                }
            }
            else
            {/*Not a control sequence, push it back*/
                ungetc(c, stdin);
            }
            break;
        case '\n':
            break;
        default:
            /*Regular character*/
            current=realloc(current, len+2);
            *(char**)ugb_fifo_index(history, history_line)=current;
            memmove(
                current+cursor+1,
                current+cursor,
                len-cursor+1
            );
            current[cursor]=c;
            cursor++;
            break;
        }
    }
    while(c!='\n'&&exit_status!=0);
    /*Return the old settings*/
    tcsetattr(STDIN_FILENO, TCSANOW, &old_term);
    putchar('\n');
    if(history_line!=0)
    {
        char *command=NULL;
        /*Remove the unused head*/
        ugb_fifo_erase(history, 0, NULL);
        history_line--;
        /*Move the selected command to the top*/
        ugb_fifo_erase(history, history_line, &command);
        ugb_fifo_push(history, &command);
    }
    return exit_status;
}
#else
static void readline_signal_handler(int signal)
{
    /*SIGINT is disabled for now*/
}
/*Crappy default that doesn't have proper arrow key support.
  NOTE: setjmp is not a good idea, it messes with SDL in some weird way.*/
unsigned readline(
    const char *prompt,
    struct ugb_fifo* history
){
    char *command=malloc(1);
    signal_handler old_signal_handler=signal(SIGINT, readline_signal_handler);
    int i=0;
    int c=0;
    unsigned exit_status=1;
    command[0]='\0';
    fputs(prompt, stdout);
    while((c=getchar())!='\n')
    {
        if(c==EOF||c=='\4')
        {
            free(command);
            command=NULL;
            exit_status=0;
            break;
        }
        command=realloc(command, i+2);
        command[i]=(char)c;
        command[i+1]='\0';
        i++;
    }
    signal(SIGINT, old_signal_handler);
    if(command!=NULL)
    {
        push_history(history, command);
    }
    return exit_status;
}
#endif

/*Returns the position of the first non-whitespace element in the string.*/
static const char * skip_whitespace(const char *str)
{
    if(str==NULL)
    {
        return str;
    }
    while(isspace(*str))
    {
        str++;
    }
    return str;
}
/*Returns the length of the first printf-style format specifier.
  fmt must begin with the format specifier. Returns 0 if the format specifier is
  ill-formed.*/
static unsigned format_specifier_length(const char *fmt)
{
    unsigned len=0;
    if(*fmt=='%')
    {
        fmt++;len++;
    }
    else
    {
        return 0;
    }
    while(1)
    {
        switch(*fmt)
        {
        case 'i':
        case 'd':
        case 'u':
        case 'o':
        case 'x':
        case 'f':
        case 'e':
        case 'g':
        case 'a':
        case 'c':
        case 's':
        case 'p':
        case 'n':
        case ']':
            len++;
            return len;
        case '[':
            while(1)
            {
                if(*fmt==']')
                {
                    break;
                }
                else if(*fmt=='\0')
                {
                    return 0;
                }
                fmt++;
                len++;
            }
            break;
        case '\0':
            return 0;
        default:
            fmt++;len++;
            break;
        }
    }
}
/*Reads 'str', attempting to match its contents to 'fmt'. On failure, 0 is
  returned. Does not care about whitespace in the beginning and the end of the
  string. Any whitespace in the middle is interpreted as 1 or more whitespace in
  the source string.
  Formatting:
    Any regular character is matched.
    Any leading and trailing whitespace in the source string is disregarded.
    Any whitespace in the format string is interpreted as 1 or more whitespace
        in the source string.
    Uses regular scanf formatting.
  Example:
    int x=0;
    match("  step \t 128  \n", "step %d", &x);
  x is now 128 and match returned 1.*/
static unsigned match(const char *str, const char *fmt, ...)
{
    va_list vl;
    unsigned success=0;
    if(str==NULL)
    {
        return 0;
    }
    va_start(vl, fmt);

    str=skip_whitespace(str);
    while(1)
    {
        if(isspace(*fmt))
        {
            if(isspace(*str))
            {
                str=skip_whitespace(str);
                fmt=skip_whitespace(fmt);
            }
            else
            {
                break;
            }
        }
        else if(*fmt=='%')
        {
            if(fmt[1]=='%')
            {
                fmt++;
                if(*fmt==*str)
                {
                    fmt++;
                    str++;
                }
                else
                {
                    break;
                }
            }
            else
            {
                unsigned characters=0;
                unsigned fmt_len=format_specifier_length(fmt);
                char *scanf_fmt=malloc(fmt_len+3);
                sprintf(scanf_fmt, "%.*s%%n", fmt_len, fmt);
                if(sscanf(str, scanf_fmt, va_arg(vl, void*), &characters)<0)
                {
                    free(scanf_fmt);
                    break;
                }
                free(scanf_fmt);
                str+=characters;
                fmt+=fmt_len;
            }
        }
        else if(*fmt=='\0')
        {
            str=skip_whitespace(str);
            if(*str==*fmt)
            {
                success=1;
            }
            break;
        }
        else if(*fmt==*str)
        {
            fmt++;str++;
        }
        else
        {
            break;
        }
    }
    va_end(vl);
    return success;
}

static void opcode_string(uint8_t opcode[3], uint16_t pc, char *str)
{
    switch(opcode[0])
    {
    case 0xCB:
        sprintf(str, ugb_instruction_cb_strings[opcode[1]]);
        break;
    case 0x01:
    case 0x08:
    case 0x11:
    case 0x21:
    case 0x31:
    case 0xC2:
    case 0xC3:
    case 0xC4:
    case 0xCA:
    case 0xCC:
    case 0xCD:
    case 0xD2:
    case 0xD4:
    case 0xDA:
    case 0xDC:
    case 0xEA:
    case 0xFA:
        /*two bytes of immediate values*/
        sprintf(
            str,
            ugb_instruction_strings[opcode[0]],
            opcode[1]|((uint16_t)opcode[2]<<8)
        );
        break;
    case 0x06:
    case 0x0E:
    case 0x16:
    case 0x1E:
    case 0x26:
    case 0x2E:
    case 0x36:
    case 0x3E:
    case 0xC6:
    case 0xCE:
    case 0xD6:
    case 0xDE:
    case 0xE0:
    case 0xE6:
    case 0xEE:
    case 0xD0:
    case 0xF0:
    case 0xF6:
    case 0xFE:
        /*one byte immediate value*/
        sprintf(str, ugb_instruction_strings[opcode[0]], opcode[1]);
        break;
    case 0xE8:
    case 0xF8:
        /*One byte signed immediate value*/
        sprintf(
            str,
            ugb_instruction_strings[opcode[0]],
            (int)(int8_t)opcode[1]
        );
        break;
    case 0x18:
    case 0x20:
    case 0x28:
    case 0x30:
    case 0x38:
        /*JR*/
        sprintf(
            str,
            ugb_instruction_strings[opcode[0]],
            pc+2+(int8_t)opcode[1]
        );
        break;
    default:
        sprintf(str, ugb_instruction_strings[opcode[0]]);
        break;
    }
}

static void print_state(struct ugb_state* state, unsigned verbosity)
{
    char instruction_str[UGB_INSTRUCTION_MAX_STRLEN]={0};
    if(verbosity>=4)
    {
        ugb_print_mbc_state(&state->mbc);
    }
    if(verbosity>=3)
    {
        fputs("CPU: ", stdout);
        if(state->cpu.halted)
        {
            fputs("HALT ", stdout);
        }
        if(state->cpu.stopped)
        {
            fputs("STOP ", stdout);
        }
        if(!state->cpu.int_master)
        {
            fputs("DI ", stdout);
        }
        putchar('\n');
    }
    if(verbosity>=2)
    {
        printf("A=0x%.2X|B=0x%.2X|C=0x%.2X|D=0x%.2X|E=0x%.2X|H=0x%.2X|L=0x%.2X|"
            "F=%c%c%c%c|SP=",
            state->cpu.a, state->cpu.b, state->cpu.c, state->cpu.d,
            state->cpu.e, state->cpu.h, state->cpu.l,
            state->cpu.f&UGB_ZERO_FLAG?'Z':'-',
            state->cpu.f&UGB_SUBTRACT_FLAG?'N':'-',
            state->cpu.f&UGB_HALF_CARRY_FLAG?'H':'-',
            state->cpu.f&UGB_CARRY_FLAG?'C':'-'
        );
        /*Colorize SP*/
        if(ugb_sp_valid(state->cpu.sp))
        {
            fputs(COLOR_GREEN, stdout);
        }
        else
        {
            fputs(COLOR_RED, stdout);
        }
        printf("0x%.4X" COLOR_DEFAULT, state->cpu.sp);
        putchar('\n');
    }
    if(verbosity>=1)
    {
        /*Colorize PC*/
        if(ugb_pc_valid(state->cpu.pc))
        {
            fputs(COLOR_GREEN, stdout);
        }
        else
        {
            fputs(COLOR_RED, stdout);
        }
        printf("0x%.4X" COLOR_DEFAULT, state->cpu.pc);
        fputs(": ", stdout);
        /*Colorize instruction*/
        if(!ugb_instruction_valid(state->opcode[0]))
        {
            fputs(COLOR_RED, stdout);
        }
        /*Get instruction string*/
        opcode_string(
            state->opcode,
            state->cpu.pc,
            instruction_str
        );
        /*Print instruction*/
        printf("%s" COLOR_DEFAULT, instruction_str);
        putchar('\n');
    }
}
static void get_opcode(
    const struct ugb_system* sys,
    uint16_t address,
    uint8_t *opcode
){
    opcode[0]=ugb_get8(sys, address);
    switch(opcode[0])
    {
    case 0x01:
    case 0x08:
    case 0x11:
    case 0x21:
    case 0x31:
    case 0xC2:
    case 0xC3:
    case 0xC4:
    case 0xCA:
    case 0xCC:
    case 0xCD:
    case 0xD2:
    case 0xD4:
    case 0xDA:
    case 0xDC:
    case 0xEA:
    case 0xFA:
        /*three byte opcode*/
        opcode[2]=ugb_get8(sys, address+2);
        /*FALLTHROUGHT INTENTIONAL*/
    case 0x06:
    case 0x0E:
    case 0x16:
    case 0x18:
    case 0x1E:
    case 0x20:
    case 0x26:
    case 0x28:
    case 0x2E:
    case 0x30:
    case 0x36:
    case 0x38:
    case 0x3E:
    case 0xC6:
    case 0xCB:
    case 0xCE:
    case 0xD6:
    case 0xDE:
    case 0xE0:
    case 0xE6:
    case 0xE8:
    case 0xEE:
    case 0xD0:
    case 0xF0:
    case 0xF6:
    case 0xF8:
    case 0xFE:
        /*two byte opcode*/
        opcode[1]=ugb_get8(sys, address+1);
        break;
    default:
        break;
    }
}

static struct ugb_state get_state(const struct ugb_system* sys)
{
    struct ugb_state state;
    state.mbc=sys->cart->mbc;
    state.cpu=sys->cpu;
    get_opcode(sys, sys->cpu.pc, state.opcode);
    return state;
}
static unsigned log_step(struct ugb_debug* debug)
{
    unsigned ret=ugb_step(debug->sys);
    struct ugb_state state=get_state(debug->sys);
    ugb_fifo_push(&debug->state_history, &state);
    return ret;
}
void ugb_instruction_string(
    const struct ugb_system* sys,
    uint16_t address,
    char *str
){
    uint8_t opcode[3]={0};
    get_opcode(sys, address, opcode);
    opcode_string(opcode, sys->cpu.pc, str);
}
unsigned ugb_pc_valid(uint16_t pc)
{
    return (/*pc>=UGB_ROM_BANK0_ADDRESS&&*/pc<UGB_HEADER_ADDRESS)||
           (pc>=UGB_EXECUTABLE_ADDRESS&&pc<UGB_TILE_RAM_ADDRESS)||
           (pc>=UGB_INT_RAM_ADDRESS&&pc<UGB_ECHO_RAM_ADDRESS)||
           (pc>=UGB_ZERO_PAGE_ADDRESS&&pc<UGB_INTERRUPT_ENABLE_ADDRESS);
}
unsigned ugb_sp_valid(uint16_t sp)
{
    return (sp>=UGB_TILE_RAM_ADDRESS&&sp<UGB_EXT_RAM_BANK_ADDRESS)||
           (sp>=UGB_INT_RAM_ADDRESS&&sp<UGB_ECHO_RAM_ADDRESS)||
           (sp>=UGB_OAM_ADDRESS&&sp<UGB_RESERVED_ADDRESS)||
           (sp>=UGB_ZERO_PAGE_ADDRESS&&sp<UGB_INTERRUPT_ENABLE_ADDRESS);
}
unsigned ugb_instruction_valid(uint8_t opcode)
{
    switch(opcode)
    {
    case 0xD3:
    case 0xDB:
    case 0xDD:
    case 0xE3:
    case 0xE4:
    case 0xEB:
    case 0xEC:
    case 0xED:
    case 0xF4:
    case 0xFC:
    case 0xFD:
        return 0;
    };
    return 1;
}
void ugb_print_system_state(const struct ugb_system* sys, unsigned verbosity)
{
    struct ugb_state state=get_state(sys);
    print_state(&state, verbosity);
}
void ugb_print_mbc_state(const struct ugb_mbc* mbc)
{
    fputs("RAM ", stdout);
    if(mbc->ram_enable)
    {
        printf(COLOR_GREEN "ON (%lu/%lu)", mbc->ram_bank+1, mbc->ram_sz);
    }
    else
    {
        fputs(COLOR_RED "OFF", stdout);
    }
    printf(COLOR_DEFAULT " / ROM (%lu/%lu)", mbc->rom_bank+1, mbc->rom_sz);
    switch(mbc->type)
    {
    case UGB_MBC1:
        printf(
            " / MBC1: %s\n",
            mbc->data.mbc1.rom_ram_mode==0?"ROM MODE":"RAM MODE"
        );
        break;
    default:
        putchar('\n');
        break;
    }
}
void ugb_print_cartridge_info(const struct ugb_cartridge* cart)
{
    size_t y=0, x=0, i=0;
    unsigned old_licensee=0;
    printf("CARTRIDGE INFO:\n");
    if(cart==NULL||cart->rom==NULL)
    {
        printf("\tCartridge invalid\n");
        return;
    }
    printf("\tROM banks: %lu\n", cart->mbc.rom_sz);
    printf("\tRAM banks: %lu\n", cart->mbc.ram_sz);
    printf("\tMBC type: ");
    switch(cart->mbc.type)
    {
    case UGB_NO_MBC:
        printf("NO MBC\n");
        break;
    case UGB_MBC1:
        printf("MBC1\n");
        break;
    case UGB_MBC2:
        printf("MBC2\n");
        break;
    case UGB_MBC3:
        printf("MBC3\n");
        break;
    case UGB_MBC5:
        printf("MBC5\n");
        break;
    }
    printf("\tLogo: \n");
    for(y=0;y<8;++y)/*Loop y pixels*/
    {
        putchar('\t');
        for(x=0;x<12;++x)/*Loop x pixels*/
        {
            /*The pattern for the logo is kind of complicated and not meant to 
              be iterated in this order.*/
            size_t offset=x*2+y/2+(y>3?22:0);
            /*One byte has pixels for two rows. The upper row has the higher
              nibble, the lower has the lower nibble.*/
            uint8_t nibble=cart->rom[0][UGB_LOGO_ADDRESS+offset]>>(y%2==0?4:0);
            for(i=0;i<4;++i)
            {/*Each nibble describes 4 pixels in x*/
                if(((nibble>>(3-i))&1)==1)
                {
                    fputs("█", stdout);
                }
                else
                {
                    putchar(' ');
                }
            }
        }
        putchar('\n');
    }
    printf("\tGame title: %.14s\n", cart->rom[0]+UGB_GAME_TITLE_ADDRESS);
    printf("\tGB type: ");
    
    if(cart->rom[0][UGB_GBC_FLAG_ADDRESS]==UGB_GB_GBC_FLAG)
    {
        printf("GB & GBC");
    }
    else if(cart->rom[0][UGB_GBC_FLAG_ADDRESS]==UGB_GBC_FLAG)
    {
        printf("GBC");
    }
    else if(cart->rom[0][UGB_GBC_FLAG_ADDRESS]==UGB_GB_FLAG)
    {
        printf("GB");
    }
    else
    {
        printf("Unknown");
    }
    if(cart->rom[0][UGB_SGB_FLAG_ADDRESS]==UGB_SGB_SUPPORT_FLAG)
    {
        printf(" & SGB\n");
    }
    else
    {
        putchar('\n');
    }
    printf("\tLocale: ");
    if(cart->rom[0][UGB_LOCALE_ADDRESS]==UGB_LOCALE_JAPANESE)
    {
        printf("Japanese\n");
    }
    else
    {
        printf("English\n");
    }
    printf("\tLicensee code: ");
    old_licensee=cart->rom[0][UGB_OLD_LICENSEE_ADDRESS];
    if(old_licensee==UGB_LICENSEE_IS_NEW)
    {
        printf("%.2s (new)\n", &cart->rom[0][UGB_NEW_LICENSEE_ADDRESS]);
    }
    else
    {
        printf("%.2X (old)\n", old_licensee);
    }
    printf("\tROM version number: %02X\n", cart->rom[0][UGB_ROM_VERSION_ADDRESS]);
}
uint32_t ugb_step_until(struct ugb_debug* debug, uint32_t until, ...)
{
    unsigned cycles=0;
    unsigned steps=0;
    unsigned max_cycles=0;
    unsigned max_steps=0;
    unsigned pc_value=0;
    uint16_t addr=0;
    uint8_t old_value=0;
    uint32_t exit_flags=0;
    uint16_t loop_end_address=0xFFFF;
    struct ugb_system* sys=debug->sys;
    va_list vl;
    va_start(vl, until);
    if(until&UGB_UNTIL_PC)
    {
        pc_value=va_arg(vl, unsigned);
    }
    if(until&UGB_UNTIL_STEPS)
    {
        max_steps=va_arg(vl, unsigned);
    }
    if(until&UGB_UNTIL_CYCLES)
    {
        max_cycles=va_arg(vl, unsigned);
    }
    if(until&UGB_UNTIL_CHANGE)
    {
        addr=va_arg(vl, unsigned);
        old_value=ugb_get8(sys, addr);
    }
    while(!exit_debug)
    {
        if(until&UGB_UNTIL_VBLANK&&sys->cpu.int_flag.fields.vblank)
        {
            exit_flags|=UGB_UNTIL_VBLANK;
        }
        if(until&UGB_UNTIL_LCDC&&sys->cpu.int_flag.fields.lcdc)
        {
            exit_flags|=UGB_UNTIL_LCDC;
        }
        if(until&UGB_UNTIL_TIMER_OVERFLOW&&
            sys->cpu.int_flag.fields.timer_overflow)
        {
            exit_flags|=UGB_UNTIL_TIMER_OVERFLOW;
        }
        if(until&UGB_UNTIL_SERIAL_IO_COMPLETE&&
            sys->cpu.int_flag.fields.serial_io_complete)
        {
            exit_flags|=UGB_UNTIL_SERIAL_IO_COMPLETE;
        }
        if(until&UGB_UNTIL_PAD_EVENT&&sys->cpu.int_flag.fields.pad_event)
        {
            exit_flags|=UGB_UNTIL_PAD_EVENT;
        }
        if(until&UGB_UNTIL_OUT_OF_LOOP&&sys->cpu.pc==loop_end_address)
        {
            exit_flags|=UGB_UNTIL_OUT_OF_LOOP;
        }
        if(until&UGB_UNTIL_INVALID_PC&&!ugb_pc_valid(sys->cpu.pc))
        {
            exit_flags|=UGB_UNTIL_INVALID_PC;
        }
        if(until&UGB_UNTIL_INVALID_SP&&!ugb_sp_valid(sys->cpu.sp))
        {
            exit_flags|=UGB_UNTIL_INVALID_SP;
        }
        if(until&UGB_UNTIL_INVALID_INSTRUCTION&&
            !ugb_instruction_valid(ugb_get8(sys, sys->cpu.pc)))
        {
            exit_flags|=UGB_UNTIL_INVALID_INSTRUCTION;
        }
        if(until&UGB_UNTIL_PC&&sys->cpu.pc==pc_value)
        {
            exit_flags|=UGB_UNTIL_PC;
        }
        if(until&UGB_UNTIL_STEPS&&steps>=max_steps)
        {
            exit_flags|=UGB_UNTIL_STEPS;
        }
        if(until&UGB_UNTIL_CYCLES&&cycles>=max_cycles)
        {
            exit_flags|=UGB_UNTIL_CYCLES;
        }
        if(until&UGB_UNTIL_CHANGE)
        {
            uint8_t new_value=ugb_get8(sys, addr);
            if(new_value!=old_value)
            {
                printf("0x%.4X: 0x%.2X -> 0x%.2X\n", addr, old_value, new_value);
                exit_flags|=UGB_UNTIL_CHANGE;
            }
        }
        if(exit_flags!=0)
        {
            break;
        }
        switch(ugb_get8(sys, sys->cpu.pc))
        {
        case 0x20:
        case 0x28:
        case 0x30:
        case 0x38:
            /*2-byte long conditional jump instruction*/
            if(loop_end_address==0xFFFF)
            {
                loop_end_address=sys->cpu.pc+2;
            }
        case 0xC2:
        case 0xCA:
        case 0xD2:
        case 0xDA:
            /*3-byte long conditional jump instruction*/
            if(loop_end_address==0xFFFF)
            {
                loop_end_address=sys->cpu.pc+3;
            }
            break;
        default:
            break;
        }
        cycles+=log_step(debug);
        steps++;
    }
    printf("Stepped %d steps and %d cycles\n", steps, cycles);
    return exit_flags;
}
struct ugb_debug ugb_create_debug(struct ugb_system* sys)
{
    struct ugb_debug debug;
    debug.sys=sys;
    debug.verbosity=1;
    debug.enabled=0;
    debug.command_history=ugb_create_fifo(
        UGB_DEFAULT_COMMAND_HISTORY_MAX_SZ,
        sizeof(char*)
    );
    debug.state_history=ugb_create_fifo(
        UGB_DEFAULT_STATE_HISTORY_MAX_SZ,
        sizeof(struct ugb_state)
    );
    return debug;
}
void ugb_free_debug(struct ugb_debug* debug)
{
    size_t i=0;
    for(i=0;i<ugb_fifo_size(&debug->command_history);++i)
    {
        free(*(char **)ugb_fifo_index(&debug->command_history, i));
    }
    ugb_free_fifo(&debug->command_history);
    ugb_free_fifo(&debug->state_history);
}
/*Reads and executes one command from stdin, then returns.*/
void ugb_step_debug(struct ugb_debug* debug)
{
    const char *command=NULL;
    unsigned steps=1;
    unsigned verbosity=debug->verbosity;
    unsigned command_history_max_sz=0;
    unsigned state_history_max_sz=0;
    unsigned address=0;
    char register_str[3]={0};
    char flags_str[4]={'-'};
    int value;
    char *strarg=NULL;
    signal_handler old_signal_handler=NULL;
    if(!debug->enabled)
    {
        return;
    }
    old_signal_handler=signal(SIGINT, debug_signal_handler);
    if(
        !readline(
            COLOR_BLUE "debug" COLOR_DEFAULT "> ",
            &debug->command_history
        )
    ){/*Failed to read a line.*/
        command=NULL;
    }
    else
    {
        command=*(char**)ugb_fifo_head(&debug->command_history);
        strarg=malloc(strlen(command)+1);
    }
    if(command==NULL||match(command, "exit"))
    {/*EXIT*/
        debug->enabled=0;
    }
    else if(
        command[0]=='\0'||
        match(command, "%u", &steps)||
        match(command, "s")||
        match(command, "step")||
        match(command, "s %u", &steps)||
        match(command, "step %u", &steps)
    ){/*STEP*/
        while(steps-->0&&!exit_debug)
        {
            log_step(debug);
            ugb_print_system_state(debug->sys, debug->verbosity);
        }
    }
    else if(
        match(command, "step until %[^\n]", strarg)||
        match(command, "su %[^\n]", strarg)||
        match(command, "u %[^\n]", strarg)||
        match(command, "until %[^\n]", strarg)||
        match(command, "to %[^\n]", strarg)
    ){
        /*strarg should now contain the given arguments.*/
        unsigned pc_addr=0;
        unsigned steps=0;
        unsigned cycles=0;
        if(match(strarg, "vblank"))
        {
            ugb_step_until(debug, UGB_UNTIL_VBLANK);
        }
        else if(match(strarg, "lcdc"))
        {
            ugb_step_until(debug, UGB_UNTIL_LCDC);
        }
        else if(match(strarg, "timer overflow"))
        {
            ugb_step_until(debug, UGB_UNTIL_TIMER_OVERFLOW);
        }
        else if(match(strarg, "serial io complete"))
        {
            ugb_step_until(debug, UGB_UNTIL_SERIAL_IO_COMPLETE);
        }
        else if(match(strarg, "pad event"))
        {
            ugb_step_until(debug, UGB_UNTIL_PAD_EVENT);
        }
        else if(match(strarg, "interrupt"))
        {
            ugb_step_until(debug, UGB_UNTIL_INTERRUPT);
        }
        else if(match(strarg, "out of loop"))
        {
            ugb_step_until(debug, UGB_UNTIL_OUT_OF_LOOP);
        }
        else if(match(strarg, "invalid pc"))
        {
            ugb_step_until(debug, UGB_UNTIL_INVALID_PC);
        }
        else if(match(strarg, "invalid sp"))
        {
            ugb_step_until(debug, UGB_UNTIL_INVALID_SP);
        }
        else if(match(strarg, "invalid instruction"))
        {
            ugb_step_until(debug, UGB_UNTIL_INVALID_INSTRUCTION);
        }
        else if(
            match(strarg, "invalid state")||
            match(strarg, "invalid")
        ){
            ugb_step_until(debug, UGB_UNTIL_INVALID_STATE);
        }
        else if(match(strarg, "pc %i", &pc_addr))
        {
            ugb_step_until(debug, UGB_UNTIL_PC, pc_addr);
        }
        else if(match(strarg, "steps %i", &steps))
        {
            ugb_step_until(debug, UGB_UNTIL_STEPS, steps);
        }
        else if(match(strarg, "cycles %i", &cycles))
        {
            ugb_step_until(debug, UGB_UNTIL_CYCLES, cycles);
        }
        else if(match(strarg, "change %i", &address))
        {
            ugb_step_until(debug, UGB_UNTIL_CHANGE, address);
        }
        else
        {
            printf("Ill-formed step until command\n");
        }
    }
    else if(
        match(command, "state")||
        match(command, "state %u", &verbosity)
    ){/*STATUS*/
        ugb_print_system_state(debug->sys, verbosity);
    }
    else if(match(command, "c")||match(command, "cartridge"))
    {/*CARTRIDGE*/
        ugb_print_cartridge_info(debug->sys->cart);
    }
    else if(match(command, "help"))
    {/*HELP*/
        printf(
            "Commands:\n"
                "\tcartridge                 - Displays cartridge info\n"
                "\tcommand history size n    - Sets the amount of lines the"
                    " command history holds.\n"
                "\tget addr                  - Reads the byte at addr\n"
                "\texit                      - Exits the debugger\n"
                "\thelp                      - Shows this help\n"
                "\tpast n                    - Shows the past n steps from"
                    " the state history\n"
                "\treset                     - Reset the GB\n"
                "\tset addr value            - Sets the byte at addr to value\n"
                "\tset register value        - Sets register to value. register"
                    " is one of the following:\n"
                "\t\ta, b, c, d, e, h, l, hl, sp, pc\n"
                "\tset f ZNHC                - Sets the flags register. ZNHC"
                    " specifies the flags.\n"
                "\tstate [verbosity]         - Shows the state of the GB. If"
                    " verbosity is not defined, the default is used.\n"
                "\tstate history size n      - Sets the amount of steps the"
                    " state history holds.\n"
                "\tstep [n]                  - Steps n steps in the program, or"
                    " 1 step if unspecified.\n"
                "\tstep until [condition1] [or [condition2] [or ...]]\n"
                "\t    - Steps until one of the conditions is met. Available"
                " conditions:\n"
                "\t\tvblank\n\t\tlcdc\n\t\ttimer overflow\n"
                "\t\tserial io complete\n\t\tpad event\n\t\tinterrupt\n"
                "\t\tout of loop\n\t\tinvalid pc\n\t\tinvalid sp\n"
                "\t\tinvalid instruction\n\t\tinvalid state (= invalid)\n"
                "\t\tpc addr (addr is the number pc must be)\n"
                "\t\tsteps n\n\t\tcycles n\n\t\tchange n\n"
                "\tverbosity                 - Shows the level of verbosity\n"
                "\tverbosity level           - Sets the level of verbosity\n"
        );
    }
    else if(match(command, "v")||match(command, "verbosity"))
    {/*VERBOSITY*/
        printf("verbosity %d\n", debug->verbosity);
    }
    else if(
        match(command, "v %u", &verbosity)||
        match(command, "verbosity %u", &verbosity)
    ){
        if(verbosity>4)
        {
            printf("verbosity must be 0-4\n");
        }
        else
        {
            debug->verbosity=verbosity;
        }
    }
    else if(
        match(command, "command history size %u", &command_history_max_sz)||
        match(command, "chs %u", &command_history_max_sz)
    ){
        size_t old_size=ugb_fifo_get_max_size(&debug->command_history);
        size_t i=command_history_max_sz;
        for(;i<old_size;++i)
        {
            free(*(char**)ugb_fifo_index(&debug->command_history, i));
        }
        ugb_fifo_set_max_size(
            &debug->command_history,
            command_history_max_sz+1
        );
    }
    else if(
        match(command, "state history size %u", &state_history_max_sz)||
        match(command, "shs %u", &state_history_max_sz)
    ){
        ugb_fifo_set_max_size(
            &debug->state_history,
            state_history_max_sz
        );
    }
    else if(match(command, "set %2s %i", register_str, &value))
    {
        if(strcmp(register_str, "a")==0)
        {
            debug->sys->cpu.a=value&0xFF;
        }
        else if(strcmp(register_str, "b")==0)
        {
            debug->sys->cpu.b=value&0xFF;
        }
        else if(strcmp(register_str, "c")==0)
        {
            debug->sys->cpu.c=value&0xFF;
        }
        else if(strcmp(register_str, "d")==0)
        {
            debug->sys->cpu.d=value&0xFF;
        }
        else if(strcmp(register_str, "e")==0)
        {
            debug->sys->cpu.e=value&0xFF;
        }
        else if(strcmp(register_str, "h")==0)
        {
            debug->sys->cpu.h=value&0xFF;
        }
        else if(strcmp(register_str, "l")==0)
        {
            debug->sys->cpu.l=value&0xFF;
        }
        else if(strcmp(register_str, "hl")==0)
        {
            debug->sys->cpu.h=(value>>8)&0xFF;
            debug->sys->cpu.l=value&0xFF;
        }
        else if(strcmp(register_str, "sp")==0)
        {
            debug->sys->cpu.sp=value&0xFFFF;
        }
        else if(strcmp(register_str, "pc")==0)
        {
            debug->sys->cpu.pc=value&0xFFFF;
        }
        else if(strcmp(register_str, "f")==0)
        {
            printf("Please use 'set f ZNHC' instead.\n");
        }
        else
        {
            printf("Unrecognized register.\n");
        }
    }
    else if(
        match(
            command,
            "set f %c%c%c%c",
            &flags_str[0],
            &flags_str[1],
            &flags_str[2],
            &flags_str[3]
        )
    ){
        debug->sys->cpu.f=0;
        debug->sys->cpu.f|=tolower(flags_str[0])=='z'?UGB_ZERO_FLAG:0;
        debug->sys->cpu.f|=tolower(flags_str[1])=='n'?UGB_SUBTRACT_FLAG:0;
        debug->sys->cpu.f|=tolower(flags_str[2])=='h'?UGB_HALF_CARRY_FLAG:0;
        debug->sys->cpu.f|=tolower(flags_str[3])=='c'?UGB_CARRY_FLAG:0;
    }
    else if(match(command, "set %i %i", &address, &value))
    {
        ugb_set8(debug->sys, address, value);
    }
    else if(match(command, "get %i", &address))
    {
        uint8_t val=ugb_get8(debug->sys, address);
        printf("0x%.2X\n", val);
    }
    else if(match(command, "reset"))
    {
        ugb_reset_system(debug->sys);
        ugb_fifo_clear(&debug->state_history);
    }
    else if(match(command, "past %u", &steps)&&steps!=0)
    {
        size_t sz=ugb_fifo_size(&debug->state_history);
        size_t i=steps<sz?steps:sz;
        for(;i>0;i--)
        {
            print_state(
                (struct ugb_state*)ugb_fifo_index(&debug->state_history, i-1),
                debug->verbosity
            );
        }
    }
    else
    {
        printf("Unrecognized command\n");
    }
    if(command!=NULL)
    {
        free(strarg);
    }
    signal(SIGINT, old_signal_handler);
    exit_debug=0;
}
