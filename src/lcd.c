/*
The MIT License (MIT)

Copyright (c) 2015 Julius Ikkala

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "lcd.h"
#include "system.h"
#include <string.h>
#include <stdlib.h>
#define sprite_height(lcd) ((lcd)->control.fields.sprite_size==1?16:8)

/*Checks if the sprite is visible on the current scanline.*/
static unsigned sprite_is_visible(
    struct ugb_lcd* lcd,
    struct ugb_sprite* s
){
    return lcd->scanline+UGB_SPRITE_Y_OFFSET>=s->pos_y
        &&lcd->scanline+UGB_SPRITE_Y_OFFSET<s->pos_y+sprite_height(lcd)
        &&s->pos_x>0
        &&s->pos_x<UGB_DISPLAY_WIDTH+UGB_SPRITE_X_OFFSET;
}
static unsigned get_pixel_from_index(
    const uint8_t *vram,
    unsigned unsigned_mode,
    uint8_t tile_index,
    uint8_t offset_x,
    uint8_t offset_y
){
    /*Get a pointer to the tile pattern data*/
    const uint8_t *pattern=NULL;
    if(unsigned_mode)
    {
        pattern=vram+(uint16_t)tile_index*UGB_TILE_BYTES;
    }
    else
    {
        int8_t signed_tile_index=tile_index;
        pattern=vram+0x1000+(int16_t)signed_tile_index*UGB_TILE_BYTES;
    }
    /*The pixels are ordered in the memory such that
      00110011
      01100100 == 0 2 3 1 0 2 1 1*/
    uint8_t offset=UGB_TILE_WIDTH-1-offset_x;
    uint8_t low=(pattern[offset_y*2]>>offset)&1;
    uint8_t high=(pattern[offset_y*2+1]>>offset)&1;
    return (high<<1)|low;
}
/*x and y are the coordinates inside the tile map.*/
static unsigned get_pixel(
    const uint8_t *vram,
    const uint8_t *tile_map,
    unsigned unsigned_mode,
    uint8_t x,
    uint8_t y
){
    /*Get the index of the tile*/
    uint8_t tile_index=tile_map[(y/8)*UGB_BACKGROUND_WIDTH+(x/8)];
    /*Coordinates of the pixel inside the tile*/
    uint8_t offset_x=x%8;
    uint8_t offset_y=y%8;
    return get_pixel_from_index(
        vram,
        unsigned_mode,
        tile_index,
        offset_x,
        offset_y
    );
}
static unsigned draw_window(
    struct ugb_lcd* lcd,
    unsigned x,
    const uint8_t *vram,
    unsigned* pixel
){
    const uint8_t *tile_map=vram+0x1800+
        0x400*lcd->control.fields.window_tile_map;
    /*Check if the window is visible on this pixel*/
    if(lcd->control.fields.background_display==0||
       lcd->control.fields.window_display==0||
       lcd->window_x>x+7||lcd->scanline<lcd->old_window_y)
    {
        return 0;
    }
    *pixel=get_pixel(
        vram,
        tile_map,
        lcd->control.fields.tileset_select,
        x+7-lcd->window_x,
        lcd->window_scanline
    );
    return 1;
}
static unsigned draw_background(
    struct ugb_lcd* lcd,
    unsigned x,
    const uint8_t *vram,
    unsigned* pixel
){
    const uint8_t *tile_map=vram+0x1800+
        0x400*lcd->control.fields.background_tile_map;
    /*Check if the background is visible on this pixel*/
    if(lcd->control.fields.background_display==0)
    {
        return 0;
    }
    *pixel=get_pixel(
        vram,
        tile_map,
        lcd->control.fields.tileset_select,
        lcd->scroll_x+x,
        lcd->scroll_y+lcd->scanline
    );
    return 1;
}
static unsigned draw_sprite(
    struct ugb_lcd* lcd,
    unsigned x,
    const uint8_t *vram,
    unsigned* pixel
){
    unsigned i=0;
    unsigned new_pixel=0;
    if(lcd->control.fields.sprite_display==0)
    {
        return 0;
    }
    for(i=0;i<UGB_SCANLINE_SPRITES;++i)
    {
        /*Y of this pixel relative to the top edge of the sprite*/
        int offset_y=(int)lcd->scanline
            -lcd->sprites[i].pos_y+UGB_SPRITE_Y_OFFSET;
        int offset_x=(int)x-lcd->sprites[i].pos_x+UGB_SPRITE_X_OFFSET;
        if(offset_y<0||offset_y>=sprite_height(lcd)||
           offset_x<0||offset_x>=UGB_TILE_WIDTH)
        {
            continue;
        }
        if(lcd->sprites[i].flags.y_flip)
        {
            offset_y=sprite_height(lcd)-1-offset_y;
        }
        if(lcd->sprites[i].flags.x_flip)
        {
            offset_x=UGB_TILE_WIDTH-1-offset_x;
        }
        new_pixel=get_pixel_from_index(
            vram,
            1,
            lcd->sprites[i].tile_index,
            offset_x,
            offset_y
        );
        if(new_pixel!=0&&(lcd->sprites[i].flags.priority==0||*pixel==0))
        {
            *pixel=new_pixel;
        }
        else
        {
            continue;
        }
        return lcd->sprites[i].flags.palette_index+2;
    }
    return 0;
}
static unsigned draw_pixel(struct ugb_lcd* lcd, unsigned x, const uint8_t *vram)
{
    unsigned pixel=0;
    /*0 - none, 1 - BGP, 2 - OBP0, 3- OBP1*/
    unsigned palette=0;
    unsigned sprite_palette=0;
    uint8_t palette_byte=0xFF;
    if(draw_window(lcd, x, vram, &pixel)!=0||
       draw_background(lcd, x, vram, &pixel)!=0
    ){
        palette=1;
    }
    if((sprite_palette=draw_sprite(lcd, x, vram, &pixel))!=0)
    {
        palette=sprite_palette;
    }
    switch(palette)
    {
    case 0:
        palette_byte=0xFF;
        break;
    case 1:
        palette_byte=lcd->tile_palette.byte;
        break;
    case 2:
        palette_byte=lcd->sprite_palette[0].byte;
        break;
    case 3:
        palette_byte=lcd->sprite_palette[1].byte;
        break;
    }
    return (palette_byte>>(pixel*2))&0x3;
}
static void draw_scanline(struct ugb_lcd* lcd, const uint8_t *vram)
{
    unsigned x=0;
    if(lcd->control.fields.lcd_on==1)
    {
        for(x=0;x<UGB_DISPLAY_WIDTH;++x)
        {
            lcd->display[lcd->scanline*UGB_DISPLAY_WIDTH+x]=lcd->palette[
                draw_pixel(lcd, x, vram)
            ];
        }
        if(lcd->control.fields.background_display==1&&
           lcd->control.fields.window_display==1&&
           lcd->scanline>=lcd->old_window_y)
        {
            lcd->window_scanline++;
        }
    }
    else
    {
        for(x=0;x<UGB_DISPLAY_WIDTH;++x)
        {
            lcd->display[lcd->scanline*UGB_DISPLAY_WIDTH+x]=lcd->palette[0];
        }
    }
}
static int sprite_compare(const void* a, const void* b)
{
    /*Sort small to big pos_x*/
    return ((const struct ugb_sprite*)a)->pos_x-
           ((const struct ugb_sprite*)b)->pos_x;
}
static void read_oam(struct ugb_lcd* lcd, const uint8_t *oam)
{
    struct ugb_sprite sprites[UGB_SPRITE_NUMBER];
    unsigned i=0, sprite_index=0;
    /*Because the sprites struct is tightly packed, oam can be nicely copied.*/
    memcpy(sprites, oam, UGB_OAM_SIZE);
    qsort(
        (void*)sprites,
        UGB_SPRITE_NUMBER,
        sizeof(struct ugb_sprite),
        sprite_compare
    );
    /*Pick visible sprites to add to lcd->sprites*/
    memset(lcd->sprites, 0, sizeof(lcd->sprites));
    for(i=0;i<UGB_SPRITE_NUMBER;++i)
    {
        if(sprites[i].pos_x!=0&&sprite_is_visible(lcd, &sprites[i]))
        {
            lcd->sprites[sprite_index++]=sprites[i];
            if(sprite_index>=UGB_SCANLINE_SPRITES)
            {
                break;
            }
        }
    }
}
static void clear_display(struct ugb_lcd* lcd)
{
    unsigned i=0;
    for(i=0;i<UGB_DISPLAY_WIDTH*UGB_DISPLAY_HEIGHT;++i)
    {
        lcd->display[i]=lcd->palette[0];
    }
}
static void next_scanline(
    struct ugb_lcd* lcd,
    struct ugb_system* sys
){
    lcd->scanline=(lcd->scanline+1)%UGB_SCANLINES;
    if(lcd->interrupt_scanline==lcd->scanline)
    {
        lcd->status.fields.scanline_coincidence=1;
        if(lcd->status.fields.scanline_coincidence_enabled)
        {
            sys->cpu.int_flag.fields.lcdc=1;
        }
    }
    else
    {
        lcd->status.fields.scanline_coincidence=0;
    }
}
struct ugb_lcd ugb_create_lcd(const struct ugb_color palette[4])
{
    struct ugb_lcd lcd;
    memset(&lcd, 0, sizeof(struct ugb_lcd));
    lcd.display=malloc(UGB_DISPLAY_SIZE);
    memcpy(lcd.palette, palette, sizeof(struct ugb_color)*4);
    clear_display(&lcd);
    memset(lcd.sprites, 0, sizeof(lcd.sprites));
    
    lcd.cycles_left=UGB_SCANLINE_CYCLES;
    lcd.scanline=0;
    lcd.control.byte=0x91;
    lcd.status.fields.mode=UGB_VBLANK;
    lcd.tile_palette.byte=0xFC;
    lcd.sprite_palette[0].byte=0xFF;
    lcd.sprite_palette[1].byte=0xFF;
    return lcd;
}
void ugb_reset_lcd(struct ugb_lcd* lcd)
{
    clear_display(lcd);
    memset(lcd->sprites, 0, sizeof(lcd->sprites));
    lcd->cycles_left=UGB_SCANLINE_CYCLES;
    lcd->scanline=0;
    lcd->interrupt_scanline=0;
    lcd->window_scanline=0;
    lcd->old_window_y=0;
    lcd->window_y=0;
    lcd->window_x=0;
    lcd->scroll_y=0;
    lcd->scroll_x=0;
    lcd->control.byte=0x91;
    lcd->status.fields.mode=UGB_VBLANK;
    lcd->tile_palette.byte=0xFC;
    lcd->sprite_palette[0].byte=0xFF;
    lcd->sprite_palette[1].byte=0xFF;
}
void ugb_free_lcd(struct ugb_lcd* lcd)
{
    if(lcd->display!=NULL)
    {
        free(lcd->display);
        lcd->display=NULL;
    }
}
void ugb_lcd_power(struct ugb_lcd* lcd, unsigned lcd_on)
{
    if(lcd_on==0)
    {
        lcd->scanline=0;
        lcd->status.byte=0;
    }
    else
    {
        lcd->cycles_left=0;
        lcd->status.fields.mode=UGB_VBLANK;
        if(lcd->scanline==lcd->interrupt_scanline)
        {
            lcd->status.fields.scanline_coincidence=1;
        }
    }
    lcd->control.fields.lcd_on=lcd_on;
}
void ugb_lcd_update(
    struct ugb_lcd* lcd,
    struct ugb_system* sys,
    unsigned cycles
){
    if(lcd->control.fields.lcd_on==0)
    {/*LCD is off, don't do anything.*/
        return;
    }
    if(cycles>lcd->cycles_left)
    {
        cycles-=lcd->cycles_left;
        /*Mode will change (or scanline if in VBLANK)*/
        /*Handle mode exit*/
        switch(lcd->status.fields.mode)
        {
        case UGB_HBLANK:
            if(lcd->scanline==UGB_DISPLAY_HEIGHT-1)
            {
                lcd->status.fields.mode=UGB_VBLANK;
            }
            else
            {
                lcd->status.fields.mode=UGB_LCDC_OAM;
            }
            break;
        case UGB_VBLANK:
            if(lcd->scanline<UGB_DISPLAY_HEIGHT)
            {/*Scanline not in VBLANK, fix this.*/
                lcd->scanline=UGB_SCANLINES-1;
            }
            if(lcd->scanline==UGB_SCANLINES-1)
            {
                lcd->window_scanline=0;
                lcd->old_window_y=lcd->window_y;
                lcd->status.fields.mode=UGB_LCDC_OAM;
            }
            break;
        case UGB_LCDC_OAM:
            lcd->status.fields.mode=UGB_LCDC_VRAM_OAM;
            break;
        case UGB_LCDC_VRAM_OAM:
            lcd->status.fields.mode=UGB_HBLANK;
            break;
        }
        /*Handle mode enter*/
        switch(lcd->status.fields.mode)
        {
        case UGB_HBLANK:
            lcd->cycles_left=UGB_HBLANK_CYCLES;
            if(lcd->status.fields.interrupt_mode00)
            {
                sys->cpu.int_flag.fields.lcdc=1;
            }
            break;
        case UGB_VBLANK:
            next_scanline(lcd, sys);
            /*This is set as such in order to let scanline proceed.*/
            lcd->cycles_left=UGB_SCANLINE_CYCLES;
            /*If first line of VBLANK*/
            if(lcd->scanline==UGB_DISPLAY_HEIGHT)
            {
                /*Trigger vblank interrupt*/
                sys->cpu.int_flag.fields.vblank=1;
                if(lcd->status.fields.interrupt_mode01)
                {
                    sys->cpu.int_flag.fields.lcdc=1;
                }
            }
            break;
        case UGB_LCDC_OAM:
            next_scanline(lcd, sys);
            lcd->cycles_left=UGB_LCDC_OAM_CYCLES;
            read_oam(lcd, sys->mem->oam);
            if(lcd->status.fields.interrupt_mode10)
            {
                sys->cpu.int_flag.fields.lcdc=1;
            }
            break;
        case UGB_LCDC_VRAM_OAM:
            lcd->cycles_left=UGB_LCDC_VRAM_OAM_CYCLES;
            draw_scanline(lcd, sys->mem->vram);
            break;
        }
    }
    lcd->cycles_left-=cycles;
}
