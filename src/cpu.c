/*
The MIT License (MIT)

Copyright (c) 2015 Julius Ikkala

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "cpu.h"
#include "system.h"
#include <stdio.h>
#include <assert.h>
#define to16(h, l) ((((uint16_t)h)<<8)|(l&0xFF))
#define carried_bits(a, b, ab) ((a)^(b)^(ab))
#define zero8(ab) (((ab)&0xFF)==0?UGB_ZERO_FLAG:0)
/*#define zero16(ab) (((ab)&0xFFFF)==0?UGB_ZERO_FLAG:0)*/
#define half_carry8(a, b, ab) \
    (carried_bits(a, b, ab)&0x10?UGB_HALF_CARRY_FLAG:0)
#define carry8(a, b, ab) (carried_bits(a, b, ab)&0x100?UGB_CARRY_FLAG:0)
#define half_carry16(a, b, ab) \
    (carried_bits(a, b, ab)&0x1000?UGB_HALF_CARRY_FLAG:0)
#define carry16(a, b, ab) (carried_bits(a, b, ab)&0x10000?UGB_CARRY_FLAG:0)

struct execute_context
{
    struct ugb_system* sys;
    unsigned cycles;
};
static void cycle(struct execute_context* ctx)
{
    ugb_timer_cycle(&ctx->sys->timer, ctx->sys);
    ctx->cycles++;
}
static uint8_t get8(struct execute_context* ctx, uint16_t addr)
{
    cycle(ctx);
    return ugb_get8(ctx->sys, addr);
}
static uint16_t get16(struct execute_context* ctx, uint16_t addr)
{
    uint8_t l=get8(ctx, addr);
    uint8_t h=get8(ctx, addr+1);
    return l|((uint16_t)h<<8);
}
static void set8(struct execute_context* ctx, uint16_t addr, uint8_t val)
{
    cycle(ctx);
    ugb_set8(ctx->sys, addr, val);
}
static void set16(struct execute_context* ctx, uint16_t addr, uint16_t val)
{
    set8(ctx, addr, val&0xFF);
    set8(ctx, addr+1, val>>8);
}
static uint8_t imm8(struct execute_context* ctx)
{
    return get8(ctx, ctx->sys->cpu.pc++);
}
static uint16_t imm16(struct execute_context* ctx)
{
    uint16_t res=get16(ctx, ctx->sys->cpu.pc);
    ctx->sys->cpu.pc+=2;
    return res;
}
static uint8_t get_reg(struct execute_context* ctx, unsigned index)
{
    switch(index&0x07)
    {
    case 0:/*B*/
        return ctx->sys->cpu.b;
    case 1:/*C*/
        return ctx->sys->cpu.c;
    case 2:/*D*/
        return ctx->sys->cpu.d;
    case 3:/*E*/
        return ctx->sys->cpu.e;
    case 4:/*H*/
        return ctx->sys->cpu.h;
    case 5:/*L*/
        return ctx->sys->cpu.l;
    case 6:/*(HL)*/
        return get8(ctx, to16(ctx->sys->cpu.h, ctx->sys->cpu.l));
    case 7:/*A*/
        return ctx->sys->cpu.a;
    }
    /*Never reached*/
    assert(0);
    return 0;
} 
static void set_reg(struct execute_context* ctx, unsigned index, uint8_t val)
{
    switch(index&0x07)
    {
    case 0:/*B*/
        ctx->sys->cpu.b=val;
        break;
    case 1:/*C*/
        ctx->sys->cpu.c=val;
        break;
    case 2:/*D*/
        ctx->sys->cpu.d=val;
        break;
    case 3:/*E*/
        ctx->sys->cpu.e=val;
        break;
    case 4:/*H*/
        ctx->sys->cpu.h=val;
        break;
    case 5:/*L*/
        ctx->sys->cpu.l=val;
        break;
    case 6:/*(HL)*/
        set8(ctx, to16(ctx->sys->cpu.h, ctx->sys->cpu.l), val);
        break;
    case 7:/*A*/
        ctx->sys->cpu.a=val;
        break;
    }
}
/*r == register
  i == immediate
  p == indirect register
  w == prefix for 16-bit value
 
  Any character twice in a row means combined 16-bit. All of the functions
  return the amount of cycles the instruction took.*/
/*==============================================================================
 * INC & DEC
 *============================================================================*/
static void inc_r(struct execute_context* ctx, uint8_t* r)
{
    unsigned tmp=*r+1;
    ctx->sys->cpu.f=zero8(tmp)|half_carry8(*r, 1, tmp)|
        (ctx->sys->cpu.f&UGB_CARRY_FLAG);
    *r=tmp;
}
static void inc_rr(struct execute_context* ctx, uint8_t* h, uint8_t* l)
{
    uint16_t tmp=to16(*h, *l)+1;
    *h=tmp>>8;
    *l=tmp&0xFF;
    cycle(ctx);
}
static void inc_sp(struct execute_context* ctx)
{
    ctx->sys->cpu.sp++;
    cycle(ctx);
}
static void inc_hl(struct execute_context* ctx)
{
    uint16_t addr=to16(ctx->sys->cpu.h, ctx->sys->cpu.l);
    uint8_t tmp=get8(ctx, addr);
    inc_r(ctx, &tmp);
    set8(ctx, addr, tmp);
}
static void dec_r(struct execute_context* ctx, uint8_t* r)
{
    unsigned tmp=*r-1;
    ctx->sys->cpu.f=zero8(tmp)|UGB_SUBTRACT_FLAG|half_carry8(*r, 1, tmp)|
        (ctx->sys->cpu.f&UGB_CARRY_FLAG);
    *r=tmp;
}
static void dec_rr(struct execute_context* ctx, uint8_t* h, uint8_t* l)
{
    uint16_t tmp=to16(*h, *l)-1;
    *h=tmp>>8;
    *l=tmp&0xFF;
    cycle(ctx);
}
static void dec_sp(struct execute_context* ctx)
{
    ctx->sys->cpu.sp--;
    cycle(ctx);
}
static void dec_hl(struct execute_context* ctx)
{
    uint16_t addr=to16(ctx->sys->cpu.h, ctx->sys->cpu.l);
    uint8_t tmp=get8(ctx, addr);
    dec_r(ctx, &tmp);
    set8(ctx, addr, tmp);
}
/*==============================================================================
 * ADD & SUB
 *============================================================================*/
static void add_r(struct execute_context* ctx, uint8_t r)
{
    unsigned tmp=ctx->sys->cpu.a+r;
    ctx->sys->cpu.f=zero8(tmp)|half_carry8(ctx->sys->cpu.a, r, tmp)|
        carry8(ctx->sys->cpu.a, r, tmp);
    ctx->sys->cpu.a=tmp;
}
static void adc_r(struct execute_context* ctx, uint8_t r)
{
    unsigned carry=(ctx->sys->cpu.f&UGB_CARRY_FLAG?1:0);
    unsigned added=r+carry;
    unsigned tmp=ctx->sys->cpu.a+added;
    unsigned carried=carry^r^ctx->sys->cpu.a^tmp;
    ctx->sys->cpu.f=zero8(tmp)|
        ((carried&0x10)==0x10?UGB_HALF_CARRY_FLAG:0)|
        ((carried&0x100)==0x100?UGB_CARRY_FLAG:0);
    ctx->sys->cpu.a=tmp;
}
static void sub_r(struct execute_context* ctx, uint8_t r)
{
    unsigned tmp=ctx->sys->cpu.a-r;
    ctx->sys->cpu.f=zero8(tmp)|UGB_SUBTRACT_FLAG|
        half_carry8(ctx->sys->cpu.a, r, tmp)|carry8(ctx->sys->cpu.a, r, tmp);
    ctx->sys->cpu.a=tmp;
}
static void sbc_r(struct execute_context* ctx, uint8_t r)
{
    unsigned carry=(ctx->sys->cpu.f&UGB_CARRY_FLAG?1:0);
    unsigned subbed=r+carry;
    unsigned tmp=ctx->sys->cpu.a-subbed;
    unsigned carried=carry^r^ctx->sys->cpu.a^tmp;
    ctx->sys->cpu.f=zero8(tmp)|UGB_SUBTRACT_FLAG|
        ((carried&0x10)==0x10?UGB_HALF_CARRY_FLAG:0)|
        ((carried&0x100)==0x100?UGB_CARRY_FLAG:0);
    ctx->sys->cpu.a=tmp;
}
static void add_hl_wr(struct execute_context* ctx, uint16_t wr)
{
    uint16_t hl=to16(ctx->sys->cpu.h, ctx->sys->cpu.l);
    uint32_t tmp=hl+wr;
    ctx->sys->cpu.f=(ctx->sys->cpu.f&UGB_ZERO_FLAG)|half_carry16(hl, wr, tmp)|
        carry16(hl, wr, tmp);
    ctx->sys->cpu.h=tmp>>8;
    ctx->sys->cpu.l=tmp&0xFF;
    cycle(ctx);
}
static void add_sp_i(struct execute_context* ctx)
{
    int16_t off=(int8_t)imm8(ctx);
    uint32_t tmp=ctx->sys->cpu.sp+off;
    ctx->sys->cpu.f=half_carry8(ctx->sys->cpu.sp, off, tmp)|
        carry8(ctx->sys->cpu.sp, off, tmp);
    ctx->sys->cpu.sp=tmp;
    cycle(ctx);
    cycle(ctx);
}
/*==============================================================================
 * AND, XOR, OR, CP
 *============================================================================*/
static void and_r(struct execute_context* ctx, uint8_t r)
{
    ctx->sys->cpu.a&=r;
    ctx->sys->cpu.f=zero8(ctx->sys->cpu.a)|UGB_HALF_CARRY_FLAG;
}
static void xor_r(struct execute_context* ctx, uint8_t r)
{
    ctx->sys->cpu.a^=r;
    ctx->sys->cpu.f=zero8(ctx->sys->cpu.a);
}
static void or_r(struct execute_context* ctx, uint8_t r)
{
    ctx->sys->cpu.a|=r;
    ctx->sys->cpu.f=zero8(ctx->sys->cpu.a);
}
static void cp_r(struct execute_context* ctx, uint8_t r)
{
    unsigned tmp=ctx->sys->cpu.a-r;
    ctx->sys->cpu.f=zero8(tmp)|UGB_SUBTRACT_FLAG|
        half_carry8(ctx->sys->cpu.a, r, tmp)|
        carry8(ctx->sys->cpu.a, r, tmp);
}
/*==============================================================================
 * RL, RR, RLC, RRC, SLA, SRA, SWAP, SRL
 *============================================================================*/
static void rl_r(struct execute_context* ctx, uint8_t* r)
{
    uint8_t tmp=(*r<<1)|(ctx->sys->cpu.f&UGB_CARRY_FLAG?1:0);
    ctx->sys->cpu.f=zero8(tmp)|(*r&0x80?UGB_CARRY_FLAG:0);
    *r=tmp;
}
static void rr_r(struct execute_context* ctx, uint8_t* r)
{
    uint8_t tmp=(*r>>1)|(ctx->sys->cpu.f&UGB_CARRY_FLAG?0x80:0);
    ctx->sys->cpu.f=zero8(tmp)|(*r&1?UGB_CARRY_FLAG:0);
    *r=tmp;
}
static void rlc_r(struct execute_context* ctx, uint8_t* r)
{
    uint8_t tmp=(*r<<1)|(*r>>7);
    ctx->sys->cpu.f=zero8(tmp)|(*r&0x80?UGB_CARRY_FLAG:0);
    *r=tmp;
}
static void rrc_r(struct execute_context* ctx, uint8_t* r)
{
    uint8_t tmp=(*r>>1)|(*r<<7);
    ctx->sys->cpu.f=zero8(tmp)|(*r&1?UGB_CARRY_FLAG:0);
    *r=tmp;
}
static void sla_r(struct execute_context* ctx, uint8_t* r)
{
    uint8_t tmp=*r<<1;
    ctx->sys->cpu.f=zero8(tmp)|(*r&0x80?UGB_CARRY_FLAG:0);
    *r=tmp;
}
static void sra_r(struct execute_context* ctx, uint8_t* r)
{
    uint8_t tmp=(*r>>1)|(*r&0x80);
    ctx->sys->cpu.f=zero8(tmp)|(*r&0x01?UGB_CARRY_FLAG:0);
    *r=tmp;
}
static void swap_r(struct execute_context* ctx, uint8_t* r)
{
    uint8_t tmp=((*r&0xF)<<4)|((*r&0xF0)>>4);
    ctx->sys->cpu.f=zero8(tmp);
    *r=tmp;
}
static void srl_r(struct execute_context* ctx, uint8_t* r)
{
    uint8_t tmp=*r>>1;
    ctx->sys->cpu.f=zero8(tmp)|(*r&0x01?UGB_CARRY_FLAG:0);
    *r=tmp;
}
/*==============================================================================
 * BIT, RES, SET
 *============================================================================*/
static void bit_r(struct execute_context* ctx, unsigned bit, uint8_t r)
{
    ctx->sys->cpu.f=zero8(r&(1<<bit))|UGB_HALF_CARRY_FLAG|
        (ctx->sys->cpu.f&UGB_CARRY_FLAG);
}
static void res_r(struct execute_context* ctx, unsigned bit, uint8_t* r)
{
    (void)ctx;
    *r&=~(uint8_t)(1<<bit);
}
static void set_r(struct execute_context* ctx, unsigned bit, uint8_t* r)
{
    (void)ctx;
    *r|=1<<bit;
}
/*==============================================================================
 * JP, JR
 *============================================================================*/
static void jp_ii(struct execute_context* ctx, unsigned condition)
{
    uint16_t addr=imm16(ctx);
    if(condition)
    {
        ctx->sys->cpu.pc=addr;
        cycle(ctx);
    }
}
static void jr_i(struct execute_context* ctx, unsigned condition)
{
    int8_t off=imm8(ctx);
    if(condition)
    {
        ctx->sys->cpu.pc+=off;
        cycle(ctx);
    }
}
/*==============================================================================
 * PUSH & POP
 *============================================================================*/
static void push_rr(struct execute_context* ctx, uint8_t h, uint8_t l)
{
    ctx->sys->cpu.sp-=2;
    cycle(ctx);
    set16(ctx, ctx->sys->cpu.sp, to16(h, l));
}
static void pop_rr(struct execute_context* ctx, uint8_t* h, uint8_t* l)
{
    uint16_t tmp=get16(ctx, ctx->sys->cpu.sp);
    ctx->sys->cpu.sp+=2;
    *h=tmp>>8;
    *l=tmp&0xFF;
}
/*==============================================================================
 * CALL, RET, RST
 *============================================================================*/
static void call_ii(struct execute_context* ctx, unsigned condition)
{
    uint16_t addr=imm16(ctx);
    if(condition)
    {
        ctx->sys->cpu.sp-=2;
        set16(ctx, ctx->sys->cpu.sp, ctx->sys->cpu.pc);
        ctx->sys->cpu.pc=addr;
        cycle(ctx);
    }
}
static void ret(struct execute_context* ctx, unsigned condition)
{
    if(condition)
    {
        ctx->sys->cpu.pc=get16(ctx, ctx->sys->cpu.sp);
        ctx->sys->cpu.sp+=2;
        cycle(ctx);
    }
}
static void rst(struct execute_context* ctx, uint8_t r)
{
    ctx->sys->cpu.sp-=2;
    set16(ctx, ctx->sys->cpu.sp, ctx->sys->cpu.pc);
    ctx->sys->cpu.pc=r;
    cycle(ctx);
}
/*==============================================================================
 * LD
 *============================================================================*/
static void ld_r_i(struct execute_context* ctx, uint8_t* r)
{
    *r=imm8(ctx);
}
static void ld_rr_ii(struct execute_context* ctx, uint8_t* h, uint8_t *l)
{
    uint16_t tmp=imm16(ctx);
    *h=tmp>>8;
    *l=tmp&0xFF;
}
static void ld_a_wp(struct execute_context* ctx, uint16_t wp)
{
    ctx->sys->cpu.a=get8(ctx, wp);
}
static void ld_hl_i(struct execute_context* ctx)
{
    set8(ctx, to16(ctx->sys->cpu.h, ctx->sys->cpu.l), imm8(ctx));
}
static void ld_wp_a(struct execute_context* ctx, uint16_t wp)
{
    set8(ctx, wp, ctx->sys->cpu.a);
}
static void ld_ii_sp(struct execute_context* ctx)
{
    set16(ctx, imm16(ctx), ctx->sys->cpu.sp);
}
static void ldi_hl_a(struct execute_context* ctx)
{
    uint16_t addr=to16(ctx->sys->cpu.h, ctx->sys->cpu.l);
    set8(ctx, addr, ctx->sys->cpu.a);
    addr++;
    ctx->sys->cpu.h=addr>>8;
    ctx->sys->cpu.l=addr&0xFF;
}
static void ldd_hl_a(struct execute_context* ctx)
{
    uint16_t addr=to16(ctx->sys->cpu.h, ctx->sys->cpu.l);
    set8(ctx, addr, ctx->sys->cpu.a);
    addr--;
    ctx->sys->cpu.h=addr>>8;
    ctx->sys->cpu.l=addr&0xFF;
}
static void ldi_a_hl(struct execute_context* ctx)
{
    uint16_t addr=to16(ctx->sys->cpu.h, ctx->sys->cpu.l);
    ctx->sys->cpu.a=get8(ctx, addr);
    addr++;
    ctx->sys->cpu.h=addr>>8;
    ctx->sys->cpu.l=addr&0xFF;
}
static void ldd_a_hl(struct execute_context* ctx)
{
    uint16_t addr=to16(ctx->sys->cpu.h, ctx->sys->cpu.l);
    ctx->sys->cpu.a=get8(ctx, addr);
    addr--;
    ctx->sys->cpu.h=addr>>8;
    ctx->sys->cpu.l=addr&0xFF;
}
static void ld_hl_spi(struct execute_context* ctx)
{
    int16_t off=(int8_t)imm8(ctx);
    uint32_t tmp=ctx->sys->cpu.sp+off;
    ctx->sys->cpu.f=half_carry8(ctx->sys->cpu.sp, off, tmp)|
        carry8(ctx->sys->cpu.sp, off, tmp);
    cycle(ctx);
    ctx->sys->cpu.h=tmp>>8;
    ctx->sys->cpu.l=tmp&0xFF;
}
static void ld_sp_ii(struct execute_context* ctx)
{
    ctx->sys->cpu.sp=imm16(ctx);
}
static void ld_sp_hl(struct execute_context* ctx)
{
    ctx->sys->cpu.sp=to16(ctx->sys->cpu.h, ctx->sys->cpu.l);
    cycle(ctx);
}
/*==============================================================================
 * MISCELLANEOUS
 *============================================================================*/
static void daa(struct execute_context* ctx)
{
    int16_t tmp=ctx->sys->cpu.a;
    if(ctx->sys->cpu.f&UGB_SUBTRACT_FLAG)
    {/*SUB*/
        if(ctx->sys->cpu.f&UGB_HALF_CARRY_FLAG)
        {
            tmp=(tmp-0x06)&0xFF;
        }
        if(ctx->sys->cpu.f&UGB_CARRY_FLAG)
        {
            tmp-=0x60;
        }
    }
    else
    {/*ADD*/
        if((tmp&0xF)>0x9||ctx->sys->cpu.f&UGB_HALF_CARRY_FLAG)
        {
            tmp+=0x06;
        }
        if(tmp>0x9F||ctx->sys->cpu.f&UGB_CARRY_FLAG)
        {
            tmp+=0x60;
        }
    }
    ctx->sys->cpu.a=tmp;
    ctx->sys->cpu.f=zero8(ctx->sys->cpu.a)|(ctx->sys->cpu.f&UGB_SUBTRACT_FLAG)|
        (tmp&0x100||(ctx->sys->cpu.f&UGB_CARRY_FLAG)?UGB_CARRY_FLAG:0);
}
static void cpl(struct execute_context* ctx)
{
    ctx->sys->cpu.a=~ctx->sys->cpu.a;
    ctx->sys->cpu.f=(ctx->sys->cpu.f&(UGB_CARRY_FLAG|UGB_ZERO_FLAG))|
            UGB_SUBTRACT_FLAG|UGB_HALF_CARRY_FLAG;
}
static void handle_cb(struct execute_context* ctx)
{
    uint8_t opcode=imm8(ctx);
    unsigned operation=opcode>>3;
    unsigned bitop=0;
    uint8_t r=get_reg(ctx, opcode);
    if(opcode<0x40)
    {
        switch(operation)
        {
        case 0:/*RLC*/
            rlc_r(ctx, &r);
            break;
        case 1:/*RRC*/
            rrc_r(ctx, &r);
            break;
        case 2:/*RL*/
            rl_r(ctx, &r);
            break;
        case 3:/*RR*/
            rr_r(ctx, &r);
            break;
        case 4:/*SLA*/
            sla_r(ctx, &r);
            break;
        case 5:/*SRA*/
            sra_r(ctx, &r);
            break;
        case 6:/*SWAP*/
            swap_r(ctx, &r);
            break;
        case 7:/*SRL*/
            srl_r(ctx, &r);
            break;
        }
    }
    else
    {
        uint8_t b=operation&0x07;
        switch(operation>>3)
        {
        case 1:/*BIT*/
            bitop=1;
            bit_r(ctx, b, r);
            break;
        case 2:/*RES*/
            res_r(ctx, b, &r);
            break;
        case 3:/*SET*/
            set_r(ctx, b, &r);
            break;
        }
    }
    if(!bitop)
    {
        set_reg(ctx, opcode, r);
    }
}
static void handle_op(struct execute_context* ctx)
{
    uint8_t opcode=imm8(ctx);
    /*Is it HALT? (Must be handled before 0x40 to 0xBF to not be interpreted as
      LD (HL), (HL))*/
    if(opcode==0x76)
    {
        ctx->sys->cpu.halted=1;
    }
    /*Is it an instruction from 0x40 to 0xBF?*/
    else if(opcode>=0x40&&opcode<0xC0)
    {
        uint8_t src=get_reg(ctx, opcode);
        if(opcode<0x80)
        {/*LD*/
            set_reg(ctx, opcode>>3, src);
        }
        else
        {/*ADD, ADC, SUB, SBC, AND, XOR, OR, CP*/
            switch((opcode-0x80)>>3)
            {
            case 0:/*ADD*/
                add_r(ctx, src);
                break;
            case 1:/*ADC*/
                adc_r(ctx, src);
                break;
            case 2:/*SUB*/
                sub_r(ctx, src);
                break;
            case 3:/*SBC*/
                sbc_r(ctx, src);
                break;
            case 4:/*AND*/
                and_r(ctx, src);
                break;
            case 5:/*XOR*/
                xor_r(ctx, src);
                break;
            case 6:/*OR*/
                or_r(ctx, src);
                break;
            case 7:/*CP*/
                cp_r(ctx, src);
                break;
            }
        }
    }
    /*Handle the rest with a switch*/
    else
    {
        switch(opcode)
        {
        case 0x00:/*NOP*/
        case 0xD3:/*Invalid opcode*/
        case 0xDB:
        case 0xDD:
        case 0xE3:
        case 0xE4:
        case 0xEB:
        case 0xEC:
        case 0xED:
        case 0xF4:
        case 0xFC:
        case 0xFD:
            break;
        case 0x01:/*LD BC, imm16*/
            ld_rr_ii(ctx, &ctx->sys->cpu.b, &ctx->sys->cpu.c);
            break;
        case 0x02:/*LD (BC), A*/
            ld_wp_a(ctx, to16(ctx->sys->cpu.b, ctx->sys->cpu.c));
            break;
        case 0x03:/*INC BC*/
            inc_rr(ctx, &ctx->sys->cpu.b, &ctx->sys->cpu.c);
            break;
        case 0x04:/*INC B*/
            inc_r(ctx, &ctx->sys->cpu.b);
            break;
        case 0x05:/*DEC B*/
            dec_r(ctx, &ctx->sys->cpu.b);
            break;
        case 0x06:/*LD B, I*/
            ld_r_i(ctx, &ctx->sys->cpu.b);
            break;
        case 0x07:/*RLCA*/
            rlc_r(ctx, &ctx->sys->cpu.a);
            ctx->sys->cpu.f&=~UGB_ZERO_FLAG;
            break;
        case 0x08:/*LD II, SP*/
            ld_ii_sp(ctx);
            break;
        case 0x09:/*ADD HL, BC*/
            add_hl_wr(ctx, to16(ctx->sys->cpu.b, ctx->sys->cpu.c));
            break;
        case 0x0A:/*LD A, (BC)*/
            ld_a_wp(ctx, to16(ctx->sys->cpu.b, ctx->sys->cpu.c));
            break;
        case 0x0B:/*DEC BC*/
            dec_rr(ctx, &ctx->sys->cpu.b, &ctx->sys->cpu.c);
            break;
        case 0x0C:/*INC C*/
            inc_r(ctx, &ctx->sys->cpu.c);
            break;
        case 0x0D:/*DEC C*/
            dec_r(ctx, &ctx->sys->cpu.c);
            break;
        case 0x0E:/*LD C, I*/
            ld_r_i(ctx, &ctx->sys->cpu.c);
            break;
        case 0x0F:/*RRCA*/
            rrc_r(ctx, &ctx->sys->cpu.a);
            ctx->sys->cpu.f&=~UGB_ZERO_FLAG;
            break;
        case 0x10:/*STOP*/
            ctx->sys->cpu.stopped=1;
            break;
        case 0x11:/*LD DE, II*/
            ld_rr_ii(ctx, &ctx->sys->cpu.d, &ctx->sys->cpu.e);
            break;
        case 0x12:/*LD (DE), A*/
            ld_wp_a(ctx, to16(ctx->sys->cpu.d, ctx->sys->cpu.e));
            break;
        case 0x13:/*INC DE*/
            inc_rr(ctx, &ctx->sys->cpu.d, &ctx->sys->cpu.e);
            break;
        case 0x14:/*INC D*/
            inc_r(ctx, &ctx->sys->cpu.d);
            break;
        case 0x15:/*DEC D*/
            dec_r(ctx, &ctx->sys->cpu.d);
            break;
        case 0x16:/*LD D, I*/
            ld_r_i(ctx, &ctx->sys->cpu.d);
            break;
        case 0x17:/*RLA*/
            rl_r(ctx, &ctx->sys->cpu.a);
            ctx->sys->cpu.f&=~UGB_ZERO_FLAG;
            break;
        case 0x18:/*JR I*/
            jr_i(ctx, 1);
            break;
        case 0x19:/*ADD HL, DE*/
            add_hl_wr(ctx, to16(ctx->sys->cpu.d, ctx->sys->cpu.e));
            break;
        case 0x1A:/*LD A, (DE)*/
            ld_a_wp(ctx, to16(ctx->sys->cpu.d, ctx->sys->cpu.e));
            break;
        case 0x1B:/*DEC DE*/
            dec_rr(ctx, &ctx->sys->cpu.d, &ctx->sys->cpu.e);
            break;
        case 0x1C:/*INC E*/
            inc_r(ctx, &ctx->sys->cpu.e);
            break;
        case 0x1D:/*DEC E*/
            dec_r(ctx, &ctx->sys->cpu.e);
            break;
        case 0x1E:/*LD E, I*/
            ld_r_i(ctx, &ctx->sys->cpu.e);
            break;
        case 0x1F:/*RRA*/
            rr_r(ctx, &ctx->sys->cpu.a);
            ctx->sys->cpu.f&=~UGB_ZERO_FLAG;
            break;
        case 0x20:/*JR NZ, I*/
            jr_i(ctx, !(ctx->sys->cpu.f&UGB_ZERO_FLAG));
            break;
        case 0x21:/*LD HL, imm16*/
            ld_rr_ii(ctx, &ctx->sys->cpu.h, &ctx->sys->cpu.l);
            break;
        case 0x22:/*LDI (HL), A*/
            ldi_hl_a(ctx);
            break;
        case 0x23:/*INC HL*/
            inc_rr(ctx, &ctx->sys->cpu.h, &ctx->sys->cpu.l);
            break;
        case 0x24:/*INC H*/
            inc_r(ctx, &ctx->sys->cpu.h);
            break;
        case 0x25:/*DEC H*/
            dec_r(ctx, &ctx->sys->cpu.h);
            break;
        case 0x26:/*LD H, I*/
            ld_r_i(ctx, &ctx->sys->cpu.h);
            break;
        case 0x27:/*DAA*/
            daa(ctx);
            break;
        case 0x28:/*JR Z, I*/
            jr_i(ctx, ctx->sys->cpu.f&UGB_ZERO_FLAG);
            break;
        case 0x29:/*ADD HL, HL*/
            add_hl_wr(ctx, to16(ctx->sys->cpu.h, ctx->sys->cpu.l));
            break;
        case 0x2A:/*LDI A, (HL)*/
            ldi_a_hl(ctx);
            break;
        case 0x2B:/*DEC HL*/
            dec_rr(ctx, &ctx->sys->cpu.h, &ctx->sys->cpu.l);
            break;
        case 0x2C:/*INC L*/
            inc_r(ctx, &ctx->sys->cpu.l);
            break;
        case 0x2D:/*DEC L*/
            dec_r(ctx, &ctx->sys->cpu.l);
            break;
        case 0x2E:/*LD L, I*/
            ld_r_i(ctx, &ctx->sys->cpu.l);
            break;
        case 0x2F:/*CPL*/
            cpl(ctx);
            break;
        case 0x30:/*JR NC, I*/
            jr_i(ctx, !(ctx->sys->cpu.f&UGB_CARRY_FLAG));
            break;
        case 0x31:/*LD SP, imm16*/
            ld_sp_ii(ctx);
            break;
        case 0x32:/*LDD (HL), A*/
            ldd_hl_a(ctx);
            break;
        case 0x33:/*INC SP*/
            inc_sp(ctx);
            break;
        case 0x34:/*INC (HL)*/
            inc_hl(ctx);
            break;
        case 0x35:/*DEC (HL)*/
            dec_hl(ctx);
            break;
        case 0x36:/*LD (HL), I*/
            ld_hl_i(ctx);
            break;
        case 0x37:/*SCF*/
            ctx->sys->cpu.f=(ctx->sys->cpu.f&UGB_ZERO_FLAG)|UGB_CARRY_FLAG;
            break;
        case 0x38:/*JR C, I*/
            jr_i(ctx, ctx->sys->cpu.f&UGB_CARRY_FLAG);
            break;
        case 0x39:/*ADD HL, SP*/
            add_hl_wr(ctx, ctx->sys->cpu.sp);
            break;
        case 0x3A:/*LDD A, (HL)*/
            ldd_a_hl(ctx);
            break;
        case 0x3B:/*DEC SP*/
            dec_sp(ctx);
            break;
        case 0x3C:/*INC A*/
            inc_r(ctx, &ctx->sys->cpu.a);
            break;
        case 0x3D:/*DEC A*/
            dec_r(ctx, &ctx->sys->cpu.a);
            break;
        case 0x3E:/*LD A, I*/
            ld_r_i(ctx, &ctx->sys->cpu.a);
            break;
        case 0x3F:/*CCF*/
            ctx->sys->cpu.f=(ctx->sys->cpu.f&UGB_ZERO_FLAG)|
                (ctx->sys->cpu.f&UGB_CARRY_FLAG?0:UGB_CARRY_FLAG);
            break;
        case 0xC0:/*RET NZ*/
            cycle(ctx);
            ret(ctx, !(ctx->sys->cpu.f&UGB_ZERO_FLAG));
            break;
        case 0xC1:/*POP BC*/
            pop_rr(ctx, &ctx->sys->cpu.b, &ctx->sys->cpu.c);
            break;
        case 0xC2:/*JP NZ, II*/
            jp_ii(ctx, !(ctx->sys->cpu.f&UGB_ZERO_FLAG));
            break;
        case 0xC3:/*JP II*/
            jp_ii(ctx, 1);
            break;
        case 0xC4:/*CALL NZ, II*/
            call_ii(ctx, !(ctx->sys->cpu.f&UGB_ZERO_FLAG));
            break;
        case 0xC5:/*PUSH BC*/
            push_rr(ctx, ctx->sys->cpu.b, ctx->sys->cpu.c);
            break;
        case 0xC6:/*ADD I*/
            add_r(ctx, imm8(ctx));
            break;
        case 0xC7:/*RST 0x00*/
            rst(ctx, 0x00);
            break;
        case 0xC8:/*RET Z*/
            cycle(ctx);
            ret(ctx, ctx->sys->cpu.f&UGB_ZERO_FLAG);
            break;
        case 0xC9:/*RET*/
            ret(ctx, 1);
            break;
        case 0xCA:/*JP Z, II*/
            jp_ii(ctx, ctx->sys->cpu.f&UGB_ZERO_FLAG);
            break;
        case 0xCB:/*CB-prefix*/
            handle_cb(ctx);
            break;
        case 0xCC:/*CALL Z, II*/
            call_ii(ctx, ctx->sys->cpu.f&UGB_ZERO_FLAG);
            break;
        case 0xCD:/*CALL II*/
            call_ii(ctx, 1);
            break;
        case 0xCE:/*ADC I*/
            adc_r(ctx, imm8(ctx));
            break;
        case 0xCF:/*RST 0x08*/
            rst(ctx, 0x08);
            break;
        case 0xD0:/*RET NC*/
            cycle(ctx);
            ret(ctx, !(ctx->sys->cpu.f&UGB_CARRY_FLAG));
            break;
        case 0xD1:/*POP DE*/
            pop_rr(ctx, &ctx->sys->cpu.d, &ctx->sys->cpu.e);
            break;
        case 0xD2:/*JP NC, II*/
            jp_ii(ctx, !(ctx->sys->cpu.f&UGB_CARRY_FLAG));
            break;
        case 0xD4:/*CALL NC, II*/
            call_ii(ctx, !(ctx->sys->cpu.f&UGB_CARRY_FLAG));
            break;
        case 0xD5:/*PUSH DE*/
            push_rr(ctx, ctx->sys->cpu.d, ctx->sys->cpu.e);
            break;
        case 0xD6:/*SUB I*/
            sub_r(ctx, imm8(ctx));
            break;
        case 0xD7:/*RST 0x10*/
            rst(ctx, 0x10);
            break;
        case 0xD8:/*RET C*/
            cycle(ctx);
            ret(ctx, ctx->sys->cpu.f&UGB_CARRY_FLAG);
            break;
        case 0xD9:/*RETI*/
            ret(ctx, 1);
            ctx->sys->cpu.int_master=1;
            break;
        case 0xDA:/*JP C, II*/
            jp_ii(ctx, ctx->sys->cpu.f&UGB_CARRY_FLAG);
            break;
        case 0xDC:/*CALL C, II*/
            call_ii(ctx, ctx->sys->cpu.f&UGB_CARRY_FLAG);
            break;
        case 0xDE:/*SBC I*/
            sbc_r(ctx, imm8(ctx));
            break;
        case 0xDF:/*RST 0x18*/
            rst(ctx, 0x18);
            break;
        case 0xE0:/*LDH (I), A*/
            ld_wp_a(ctx, 0xFF00+imm8(ctx));
            break;
        case 0xE1:/*POP HL*/
            pop_rr(ctx, &ctx->sys->cpu.h, &ctx->sys->cpu.l);
            break;
        case 0xE2:/*LD (C), A*/
            ld_wp_a(ctx, 0xFF00+ctx->sys->cpu.c);
            break;
        case 0xE5:/*PUSH HL*/
            push_rr(ctx, ctx->sys->cpu.h, ctx->sys->cpu.l);
            break;
        case 0xE6:/*AND I*/
            and_r(ctx, imm8(ctx));
            break;
        case 0xE7:/*RST 0x20*/
            rst(ctx, 0x20);
            break;
        case 0xE8:/*ADD SP, I*/
            add_sp_i(ctx);
            break;
        case 0xE9:/*JP (HL)*/
            ctx->sys->cpu.pc=to16(ctx->sys->cpu.h, ctx->sys->cpu.l);
            break;
        case 0xEA:/*LD (II), A*/
            ld_wp_a(ctx, imm16(ctx));
            break;
        case 0xEE:/*XOR I*/
            xor_r(ctx, imm8(ctx));
            break;
        case 0xEF:/*RST 0x28*/
            rst(ctx, 0x28);
            break;
        case 0xF0:/*LDH A, (I)*/
            ld_a_wp(ctx, 0xFF00+imm8(ctx));
            break;
        case 0xF1:/*POP AF*/
            pop_rr(ctx, &ctx->sys->cpu.a, &ctx->sys->cpu.f);
            ctx->sys->cpu.f&=0xF0;
            break;
        case 0xF2:/*LD A, (C)*/
            ld_a_wp(ctx, 0xFF00+ctx->sys->cpu.c);
            break;
        case 0xF3:/*DI*/
            ctx->sys->cpu.int_master=0;
            break;
        case 0xF5:/*PUSH AF*/
            push_rr(ctx, ctx->sys->cpu.a, ctx->sys->cpu.f);
            break;
        case 0xF6:/*OR I*/
            or_r(ctx, imm8(ctx));
            break;
        case 0xF7:/*RST 0x30*/
            rst(ctx, 0x30);
            break;
        case 0xF8:/*LD HL, SP+I*/
            ld_hl_spi(ctx);
            break;
        case 0xF9:/*LD SP, HL*/
            ld_sp_hl(ctx);
            break;
        case 0xFA:/*LD A, (II),*/
            ld_a_wp(ctx, imm16(ctx));
            break;
        case 0xFB:/*EI*/
            ctx->sys->cpu.int_master=1;
            break;
        case 0xFE:/*CP I*/
            cp_r(ctx, imm8(ctx));
            break;
        case 0xFF:/*RST 0x38*/
            rst(ctx, 0x38);
            break;
        default:
            assert(0/*Unimplemented opcode*/);
            break;
        }
    }
}
unsigned ugb_execute(struct ugb_system* sys)
{
    struct execute_context ctx={sys, 0};
    if(!sys->cpu.stopped)
    {
        if(!sys->cpu.halted)
        {
            handle_op(&ctx);
        }
        else
        {
            /*Cycle the timer even when halted.*/
            cycle(&ctx);
        }
    }
    return ctx.cycles;
}
struct ugb_cpu ugb_create_cpu()
{
    struct ugb_cpu cpu;
    cpu.sp=0xFFFE;
    cpu.pc=0x100;
    cpu.a=0x01;
    cpu.f=0xB0;
    cpu.b=0x00;
    cpu.c=0x13;
    cpu.d=0x00;
    cpu.e=0xD8;
    cpu.h=0x01;
    cpu.l=0x4D;
    cpu.halted=0;
    cpu.stopped=0;
    cpu.int_master=0;
    cpu.int_flag.byte=0;
    cpu.int_enable.byte=0;
    return cpu;
}
