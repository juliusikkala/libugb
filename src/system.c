/*
The MIT License (MIT)

Copyright (c) 2015 Julius Ikkala

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "system.h"
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

static void dma_transfer(struct ugb_system* sys, uint8_t val)
{
    /*The DMA transfer reads 0xA0 bytes from addr to 0xFE00*/
    uint16_t addr=(uint16_t)val<<8;
    uint16_t dest=UGB_OAM_ADDRESS;
    unsigned bytes=0;
    if(val>=0xF2)
    {/*Starting address is too high*/
        return;
    }
    for(bytes=0;bytes<0xA0;++bytes, ++dest, ++addr)
    {
        ugb_set8(sys, dest, ugb_get8(sys, addr));
    }
}
struct ugb_system ugb_create_system(
    struct ugb_cartridge* cart,
    struct ugb_lcd* lcd
){
    struct ugb_system sys;
    memset(&sys, 0, sizeof(struct ugb_system));
    sys.mem=malloc(sizeof(struct ugb_memory));
    memset(sys.mem, 0, sizeof(struct ugb_memory));
    sys.timer=ugb_create_timer();
    sys.cpu=ugb_create_cpu();
    sys.pad=ugb_create_pad();
    sys.cart=cart;
    sys.lcd=lcd;
    sys.userdata=NULL;
    sys.save_cb=NULL;
    ugb_reset_system(&sys);
    return sys;
}
void ugb_reset_system(struct ugb_system* sys)
{
    memset(sys->mem, 0, sizeof(struct ugb_memory));
    sys->timer=ugb_create_timer();
    sys->cpu=ugb_create_cpu();
    sys->pad=ugb_create_pad();
    ugb_reset_cartridge(sys->cart);
    ugb_reset_lcd(sys->lcd);
}

void ugb_free_system(struct ugb_system* sys)
{
    if(sys==NULL)
    {
        return;
    }
    if(sys->mem!=NULL)
    {
        free(sys->mem);
    }
    memset(sys, 0, sizeof(struct ugb_system));
}

uint8_t ugb_get8(const struct ugb_system* sys, uint16_t addr)
{
    assert(sys!=NULL);
    if(addr==UGB_INTERRUPT_ENABLE_ADDRESS)
    {
        return sys->cpu.int_enable.byte;
    }
    else if(addr>=UGB_ZERO_PAGE_ADDRESS)
    {
        return sys->mem->zero_page[addr-UGB_ZERO_PAGE_ADDRESS];
    }
    else if(addr>=UGB_IO_ADDRESS)
    {
        switch(addr)
        {
        case 0xFF00:
            return sys->pad.p1.byte;
        case 0xFF01:
            return sys->serial.data;
        case 0xFF02:
            return sys->serial.control.byte;
        case 0xFF04:
            return sys->timer.divider;
        case 0xFF05:
            return sys->timer.ticks;
        case 0xFF06:
            return sys->timer.modulo;
        case 0xFF07:
            return sys->timer.control.byte;
        case 0xFF0F:
            return sys->cpu.int_flag.byte;
        case 0xFF40:
            return sys->lcd->control.byte;
        case 0xFF41:
            return sys->lcd->status.byte;
        case 0xFF42:
            return sys->lcd->scroll_y;
        case 0xFF43:
            return sys->lcd->scroll_x;
        case 0xFF44:
            return sys->lcd->scanline;
        case 0xFF45:
            return sys->lcd->interrupt_scanline;
        case 0xFF47:
            return sys->lcd->tile_palette.byte;
        case 0xFF48:
            return sys->lcd->sprite_palette[0].byte;
        case 0xFF49:
            return sys->lcd->sprite_palette[1].byte;
        case 0xFF4A:
            return sys->lcd->window_y;
        case 0xFF4B:
            return sys->lcd->window_x;
        default:
            return 0xFF;
        }
    }
    else if(addr>=UGB_RESERVED_ADDRESS)
    {
        return 0;
    }
    else if(addr>=UGB_OAM_ADDRESS)
    {
        if(sys->lcd->status.fields.mode>=2)
        {
            return 0;
        }
        else
        {
            return sys->mem->oam[addr-UGB_OAM_ADDRESS];
        }
    }
    else if(addr>=UGB_ECHO_RAM_ADDRESS)
    {
        return ugb_get8(
            sys,
            addr-UGB_ECHO_RAM_ADDRESS+UGB_INT_RAM_ADDRESS
        );
    }
    else if(addr>=UGB_INT_RAM_ADDRESS)
    {
        return sys->mem->ram[addr-UGB_INT_RAM_ADDRESS];
    }
    else if(addr>=UGB_EXT_RAM_BANK_ADDRESS)
    {
        return ugb_cartridge_read(sys->cart, addr);
    }
    else if(addr>=UGB_TILE_RAM_ADDRESS)
    {
        if(sys->lcd->status.fields.mode==3)
        {
            return 0;
        }
        else
        {
            return sys->mem->vram[addr-UGB_TILE_RAM_ADDRESS];
        }
    }
    else/*if(addr>=UGB_ROM_ADDRESS)*/
    {
        return ugb_cartridge_read(sys->cart, addr);
    }
    assert(0);
    return 0;
}
void ugb_set8(struct ugb_system* sys, uint16_t addr, uint8_t val)
{
    assert(sys!=NULL);
    if(addr==UGB_INTERRUPT_ENABLE_ADDRESS)
    {
        sys->cpu.int_enable.byte=val&0x1F;
    }
    else if(addr>=UGB_ZERO_PAGE_ADDRESS)
    {
        sys->mem->zero_page[addr-UGB_ZERO_PAGE_ADDRESS]=val;
    }
    else if(addr>=UGB_IO_ADDRESS)
    {
        switch(addr)
        {
        case 0xFF00:
            sys->pad.p1.byte=val&0x30;
            ugb_pad_update(&sys->pad, sys);
            break;
        case 0xFF01:
            sys->serial.data=0;/*TODO: serial port support*/
            break;
        case 0xFF02:
            sys->serial.control.byte=0;/*TODO: serial port support*/
            break;
        case 0xFF04:
            sys->timer.divider=0;
            break;
        case 0xFF05:
            sys->timer.ticks=val;
            break;
        case 0xFF06:
            sys->timer.modulo=val;
            break;
        case 0xFF07:
            sys->timer.control.byte=val&7;
            break;
        case 0xFF0F:
            sys->cpu.int_flag.byte=val&0x1F;
            break;
        case 0xFF40:
            if(sys->lcd->control.fields.lcd_on!=val>>7)
            {
                ugb_lcd_power(sys->lcd, val>>7);
            }
            sys->lcd->control.byte=val;
            break;
        case 0xFF41:
            sys->lcd->status.byte&=~0x78;
            sys->lcd->status.byte|=val&0x78;
            break;
        case 0xFF42:
            sys->lcd->scroll_y=val;
            break;
        case 0xFF43:
            sys->lcd->scroll_x=val;
            break;
        case 0xFF44:
            sys->lcd->scanline=val;
            break;
        case 0xFF45:
            sys->lcd->interrupt_scanline=val;
            break;
        case 0xFF46:
            /*Initiate DMA transfer*/
            dma_transfer(sys, val);
            break;
        case 0xFF47:
            sys->lcd->tile_palette.byte=val;
            break;
        case 0xFF48:
            sys->lcd->sprite_palette[0].byte=val;
            break;
        case 0xFF49:
            sys->lcd->sprite_palette[1].byte=val;
            break;
        case 0xFF4A:
            sys->lcd->window_y=val;
            break;
        case 0xFF4B:
            sys->lcd->window_x=val;
            break;
        default:
            break;
        }
    }
    else if(addr>=UGB_RESERVED_ADDRESS)
    {
        return;
    }
    else if(addr>=UGB_OAM_ADDRESS)
    {
        if(sys->lcd->status.fields.mode>=2)
        {
            return;
        }
        else
        {
            sys->mem->oam[addr-UGB_OAM_ADDRESS]=val;
        }
    }
    else if(addr>=UGB_ECHO_RAM_ADDRESS)
    {
        ugb_set8(
            sys,
            addr-UGB_ECHO_RAM_ADDRESS+UGB_INT_RAM_ADDRESS,
            val
        );
        return;
    }
    else if(addr>=UGB_INT_RAM_ADDRESS)
    {
        sys->mem->ram[addr-UGB_INT_RAM_ADDRESS]=val;
    }
    else if(addr>=UGB_EXT_RAM_BANK_ADDRESS)
    {
        ugb_cartridge_write(sys->cart, addr, val);
    }
    else if(addr>=UGB_TILE_RAM_ADDRESS)
    {
        if(sys->lcd->status.fields.mode==3)
        {
            return;
        }
        else
        {
            sys->mem->vram[addr-UGB_TILE_RAM_ADDRESS]=val;
        }
        sys->mem->vram[addr-UGB_TILE_RAM_ADDRESS]=val;
    }
    else/*if(addr>=UGB_ROM_ADDRESS)*/
    {
        unsigned ram_enable=sys->cart->mbc.ram_enable;
        ugb_cartridge_write(sys->cart, addr, val);
        if(ram_enable==1&&sys->cart->mbc.ram_enable==0)
        {/*RAM was disabled, call save callback*/
            if(sys->save_cb!=NULL)
            {
                sys->save_cb(
                    sys->userdata,
                    (const uint8_t*)sys->cart->ram,
                    UGB_RAM_BANK_SIZE*sys->cart->mbc.ram_sz
                );
            }
        }
    }
}
uint16_t ugb_get16(const struct ugb_system* sys, uint16_t addr)
{
    return ugb_get8(sys, addr)|((uint16_t)ugb_get8(sys, addr+1))<<8;
}
void ugb_set16(struct ugb_system* sys, uint16_t addr, uint16_t val)
{
    uint8_t hi=val>>8, lo=val&0xFF;
    ugb_set8(sys, addr, lo);
    ugb_set8(sys, addr+1, hi);
}
void ugb_set_callbacks(
    struct ugb_system* sys,
    void* userdata,
    ugb_save_callback save_cb
){
    sys->userdata=userdata;
    sys->save_cb=save_cb;
}
/*Returns spent cycles*/
static unsigned handle_interrupts(struct ugb_system* sys)
{
    uint16_t interrupt_address=0;
    if(sys->cpu.int_enable.byte&sys->cpu.int_flag.byte)
    {
        sys->cpu.halted=0;
    }
    if(!sys->cpu.int_master)
    {
        return 0;
    }
    if(sys->cpu.int_enable.fields.vblank&&sys->cpu.int_flag.fields.vblank)
    {
        /*vblank interrupt triggered*/
        sys->cpu.int_flag.fields.vblank=0;
        interrupt_address=UGB_VBLANK_INTERRUPT_ADDRESS;
    }
    else if(sys->cpu.int_enable.fields.lcdc&&sys->cpu.int_flag.fields.lcdc)
    {
        /*lcdc interrupt triggered*/
        sys->cpu.int_flag.fields.lcdc=0;
        interrupt_address=UGB_LCDC_INTERRUPT_ADDRESS;
    }
    else if(sys->cpu.int_enable.fields.timer_overflow
          &&sys->cpu.int_flag.fields.timer_overflow)
    {
        /*timer overflow interrupt triggered*/
        sys->cpu.int_flag.fields.timer_overflow=0;
        interrupt_address=UGB_TIMER_OVERFLOW_INTERRUPT_ADDRESS;
    }
    else if(sys->cpu.int_enable.fields.serial_io_complete
          &&sys->cpu.int_flag.fields.serial_io_complete)
    {
        /*serial io complete interrupt triggered*/
        sys->cpu.int_flag.fields.serial_io_complete=0;
        interrupt_address=UGB_SERIAL_TRANSFER_INTERRUPT_ADDRESS;
    }
    else if(sys->cpu.int_enable.fields.pad_event
          &&sys->cpu.int_flag.fields.pad_event)
    {
        /*pad event interrupt triggered*/
        sys->cpu.int_flag.fields.pad_event=0;
        interrupt_address=UGB_PAD_EVENT_INTERRUPT_ADDRESS;
    }
    else
    {
        return 0;
    }
    ugb_timer_update(&sys->timer, sys, 5);
    sys->cpu.int_master=0;
    sys->cpu.sp-=2;
    ugb_set16(sys, sys->cpu.sp, sys->cpu.pc);
    sys->cpu.pc=interrupt_address;
    return 5;
}
unsigned ugb_step(struct ugb_system* sys)
{
    assert(sys!=NULL);
    if(sys->cpu.stopped)
    {
        return 0;
    }
    unsigned spent_cycles=0;
    spent_cycles+=handle_interrupts(sys);
    spent_cycles+=ugb_execute(sys);
    ugb_lcd_update(sys->lcd, sys, spent_cycles);
    return spent_cycles==0?1:spent_cycles;
}
void ugb_step_frame(struct ugb_system* sys)
{
    if(sys->lcd->control.fields.lcd_on==0)
    {
        size_t i=0;
        for(i=0;i<UGB_LCD_REFRESH_CYCLES&&!sys->cpu.stopped;++i)
        {
            ugb_step(sys);
        }
        return;
    }
    else
    {
        unsigned frame_cycles=0;
        /*If we're already in vblank, step until out of it.*/
        while(sys->lcd->status.fields.mode==UGB_VBLANK&&!sys->cpu.stopped&&
              sys->lcd->control.fields.lcd_on==1
        ){
            frame_cycles+=ugb_step(sys);
        }
        /*Step until mode changes to vblank*/
        while(sys->lcd->status.fields.mode!=UGB_VBLANK&&!sys->cpu.stopped&&
              sys->lcd->control.fields.lcd_on==1
        ){
            frame_cycles+=ugb_step(sys);
        }
    }
}
