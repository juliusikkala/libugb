/*
The MIT License (MIT)

Copyright (c) 2015 Julius Ikkala

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "timer.h"
#include "system.h"
#include <stdlib.h>
#include <string.h>

static uint32_t get_cycles_to_tick(unsigned clock_freq)
{
    static const uint32_t cycles_to_tick[]=
    {
        UGB_TIMER00_CYCLES,
        UGB_TIMER01_CYCLES,
        UGB_TIMER10_CYCLES,
        UGB_TIMER11_CYCLES
    };
    return cycles_to_tick[clock_freq];
}

struct ugb_timer ugb_create_timer()
{
    struct ugb_timer timer;
    memset(&timer, 0, sizeof(struct ugb_timer));
    return timer;
}
void ugb_timer_cycle(struct ugb_timer* timer, struct ugb_system* sys)
{
    /*Divider runs always, even when the timer isn't running*/
    timer->divider_cycles++;
    if(timer->divider_cycles==UGB_TIMER11_CYCLES)
    {
        timer->divider_cycles=0;
        timer->divider++;
    }
    timer->cycles++;
    if(timer->cycles>=get_cycles_to_tick(sys->timer.control.fields.clock_freq))
    {
        timer->cycles=0;
        if(timer->ticks==0xFF)
        {/*Overflow*/
            timer->ticks=timer->modulo;
            sys->cpu.int_flag.fields.timer_overflow=1;
        }
        else if(timer->control.fields.timer_run)
        {
            timer->ticks++;
        }
    }
}
void ugb_timer_update(
    struct ugb_timer* timer,
    struct ugb_system* sys,
    unsigned spent_cycles
){
    unsigned cycles=0;
    for(cycles=0;cycles<spent_cycles;++cycles)
    {
        ugb_timer_cycle(timer, sys);
    }
}
